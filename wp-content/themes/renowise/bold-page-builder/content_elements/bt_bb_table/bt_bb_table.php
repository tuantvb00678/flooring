<?php

class bt_bb_table extends BT_BB_Element {

	function handle_shortcode( $atts, $content ) {
		extract( shortcode_atts( apply_filters( 'bt_bb_extract_atts', array(
			'content'	=> ''
		) ), $atts, $this->shortcode ) );

		$class = array( $this->shortcode );

		if ( $el_class != '' ) {
			$class[] = $el_class;
		}

		$id_attr = '';
		if ( $el_id != '' ) {
			$id_attr = ' ' . 'id="' . esc_attr( $el_id ) . '"';
		}

		$style_attr = '';
		if ( $el_style != '' ) {
			$style_attr = ' ' . 'style="' . esc_attr( $el_style ) . '"';
		}

		$output_inner = '';
		$items_arr = preg_split( '/$\R?^/m', $content );
		
		foreach ( $items_arr as $item ) {
			$item = preg_replace('~[\r\n]+~', '', $item);
			$item_arr = explode( ';', $item );	
			$extra_class = "";
			
			$output_inner .= '<tr class="' . esc_attr( $this->shortcode . '_row' ) . $extra_class . '">';
				foreach ($item_arr as $item_value) {
					$output_inner .= '<td class="' . esc_attr( $this->shortcode . '_value' ) . '">';
						$output_inner .= '<span>' . $item_value . '</span>';
					$output_inner .= '</td>';
				}				
			$output_inner .= '</tr>';
		}

		$output = '<table' . $id_attr . ' class="' . implode( ' ', $class ) . '"' . $style_attr . '>' . $output_inner . '</table>';
		
		$output = apply_filters( 'bt_bb_general_output', $output, $atts );
		$output = apply_filters( $this->shortcode . '_output', $output, $atts );

		return $output;
	}

	function map_shortcode() {

		bt_bb_map( $this->shortcode, array( 'name' => esc_html__( 'Table', 'renowise' ), 'description' => esc_html__( 'Table with list of prices', 'renowise' ), 'icon' => $this->prefix_backend . 'icon' . '_' . $this->shortcode,
			'params' => array(
				array( 'param_name' => 'content', 'type' => 'textarea', 'heading' => esc_html__( 'Content', 'renowise' ), 'description' => esc_html__( 'Type title separated with ; and than price (eg. National Average;$45,376). In order to show multiple rows, separate sentences by new lines.', 'renowise' )
				)
			))
		);
	}
}