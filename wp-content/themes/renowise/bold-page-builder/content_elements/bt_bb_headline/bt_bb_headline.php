<?php

class bt_bb_headline extends BT_BB_Element {

	function handle_shortcode( $atts, $content ) {
		extract( shortcode_atts( apply_filters( 'bt_bb_extract_atts_' . $this->shortcode, array(
			'headline'      			=> '',
			'html_tag'      			=> '',
			'font'          			=> '',
			'font_subset'   			=> '',
			'size'     					=> '',
			'font_size'     			=> '',
			'font_weight'   			=> '',
			'subheadline_font_weight'   => '',
			'color_scheme'  			=> '',
			'superheadline_color'  		=> '',
			'superheadline_bg_color'  	=> '',
			'color'         			=> '',
			'subheadline_color'  		=> '',
			'supertitle_position'		=> '',
			'image'  					=> '',
			'dash'          			=> '',
			'style'         			=> '',
			'align'         			=> '',
			'url'           			=> '',
			'target'        			=> '',
			'superheadline' 			=> '',
			'subheadline'   			=> ''
		) ), $atts, $this->shortcode ) );

		$superheadline = html_entity_decode( $superheadline, ENT_QUOTES, 'UTF-8' );
		$subheadline = html_entity_decode( $subheadline, ENT_QUOTES, 'UTF-8' );
		$headline = html_entity_decode( $headline, ENT_QUOTES, 'UTF-8' );

		if ( $font != '' && $font != 'inherit' ) {
			require_once( dirname(__FILE__) . '/../../../../../plugins/bold-page-builder/content_elements_misc/misc.php' );
			bt_bb_enqueue_google_font( $font, $font_subset );
		}

		$class = array( $this->shortcode );
		
		if ( $el_class != '' ) {
			$class[] = $el_class;
		}

		$id_attr = '';
		if ( $el_id != '' ) {
			$id_attr = ' ' . 'id="' . esc_attr( $el_id ) . '"';
		}
		
		$html_tag_style = "";
		$html_tag_style_arr = array();
		if ( $font != '' && $font != 'inherit' ) {
			$el_style = $el_style . ';' . 'font-family:\'' . urldecode( $font ) . '\'';
			$html_tag_style_arr[] = 'font-family:\'' . urldecode( $font ) . '\'';
		}
		if ( $font_size != '' ) {
			$html_tag_style_arr[] = 'font-size:' . $font_size  ;
		}
		if ( count( $html_tag_style_arr ) > 0 ) {
			$html_tag_style = ' style="' . implode( $html_tag_style_arr, '; ') . '"';
		}

		$h_span_bg_style = '';
		if ( $image != '' && is_numeric( $image ) ) {
			$post_image = get_post( $image );
			if ( $post_image == '' ) return;
			$image = wp_get_attachment_image_src( $image, $size );
			$image = $image[0];
			$class[] = "btHasBgImage";
			$h_span_bg_style = "style = \"background-image:url('" . $image . "')\"";
		}
		
		if ( $font_weight != '' ) {
			$class[] = $this->prefix . 'font_weight_' . $font_weight ;
		}

		if ( $subheadline_font_weight != '' ) {
			$class[] = $this->prefix . 'subheadline_font_weight_' . $subheadline_font_weight ;
		}
		
		if ( $color_scheme != '' ) {
			$class[] = $this->prefix . 'color_scheme_' . bt_bb_get_color_scheme_id( $color_scheme );
		}

		if ( $style != '' ) {
			$class[] = $this->prefix . 'style' . '_' . $style;
		}

		if ( $color != '' ) {
			$el_style = $el_style . ';' . 'color:' . $color . ';';
		}

		if ( $dash != '' ) {
			$class[] = $this->prefix . 'dash' . '_' . $dash;
		}
		
		if ( $size != '' ) {
			$class[] = $this->prefix . 'size' . '_' . $size;
		}
		
		if ( $target == '' ) {
			$target = '_self';
		}


		// SUPERHEADLINE STYLE
		$superheadline_style_arr = '';

		if ( $superheadline_color != '' ) {
			$superheadline_style_arr = $superheadline_style_arr . ';' . 'color:' . $superheadline_color ;
		}
		if ( $superheadline_bg_color != '' ) {
			$superheadline_style_arr = $superheadline_style_arr . '; ' . 'background-color:' . $superheadline_bg_color . '; display:inline-block; padding: 0.5em 0.8em;' ;
		}


		$superheadline_style = '';
		if ( $superheadline_style_arr != '' ) {
			$superheadline_style = ' ' . 'style="' . esc_attr( $superheadline_style_arr ) . '"';
		}


		// POSITION
		$superheadline_inside = '';
		$superheadline_outside = '';
		
		if ( $superheadline != '' ) {
			$class[] = $this->prefix . 'superheadline';
			
			if ( $supertitle_position == 'outside' ) { 
				$superheadline_outside = '<span class="' . esc_attr( $this->shortcode ) . '_superheadline" ' . $superheadline_style . '>' . $superheadline . '</span>';
			} else {
				$superheadline_inside = '<span class="' . esc_attr( $this->shortcode ) . '_superheadline" ' . $superheadline_style . '>' . $superheadline . '</span>';
			}
		}
		
		if ( $subheadline != '' ) {
			$class[] = $this->prefix . 'subheadline';
			if ( $subheadline_color != '' ) {
				$subheadline = '<div class="' . esc_attr( $this->shortcode . '_subheadline' ) . '" style="color: ' . $subheadline_color . ';">' . $subheadline . '</div>';
			} else {
				$subheadline = '<div class="' . esc_attr( $this->shortcode . '_subheadline' ) . '">' . $subheadline . '</div>';
			}
			$subheadline = nl2br( $subheadline );
		}


		$style_attr = '';
		if ( $el_style != '' ) {
			$style_attr = ' ' . 'style="' . esc_attr( $el_style ) . '"';
		}

		if ( $align != '' ) {
			$class[] = $this->prefix . 'align' . '_' . $align;
		}
		
		$class = apply_filters( $this->shortcode . '_class', $class, $atts );
		
		if ( $headline != '' ) {
			if ( $url != '' ) {
				$url_title = strip_tags( str_replace( array("\n", "\r"), ' ', $headline ) );
				$link = bt_bb_get_url( $url );
				// IMPORTANT: esc_attr must be used instead of esc_url(_raw)
				$headline = '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $target ) . '" title="' . esc_attr( $url_title )  . '">' . $headline . '</a>';
			}		
			$headline = '<span class="' . esc_attr( $this->shortcode . '_content' ) . '"><span ' . $h_span_bg_style . '>' . $headline . '</span></span>';
		}
		
		$headline = nl2br( $headline );

		$output = '<header' . $id_attr . ' class="' . implode( ' ', $class ) . '"' . $style_attr . '>';

		if ( $superheadline_outside != '' ) $output .= '<div class="' . esc_attr( $this->shortcode . '_superheadline_outside' ) . '">' . $superheadline_outside . '</div>';
		if ( $headline != '' || $superheadline_inside != '' ) $output .= '<' . $html_tag . $html_tag_style . '>' . $superheadline_inside . $headline . '</' . $html_tag . '>';
		$output .= $subheadline . '</header>';
		
		$output = apply_filters( 'bt_bb_general_output', $output, $atts );
		$output = apply_filters( $this->shortcode . '_output', $output, $atts );

		return $output;

	}

	function map_shortcode() {

		require_once( dirname(__FILE__) . '/../../../../../plugins/bold-page-builder/content_elements_misc/fonts.php' );
		$color_scheme_arr = bt_bb_get_color_scheme_param_array();	

		bt_bb_map( $this->shortcode, array( 'name' => esc_html__( 'Headline', 'renowise' ), 'description' => esc_html__( 'Headline with custom Google fonts', 'renowise' ), 'icon' => $this->prefix_backend . 'icon' . '_' . $this->shortcode, 'highlight' => true,
			'params' => array(
				array( 'param_name' => 'superheadline', 'type' => 'textfield', 'heading' => esc_html__( 'Superheadline', 'renowise' ) ),
				array( 'param_name' => 'headline', 'type' => 'textarea', 'heading' => esc_html__( 'Headline', 'renowise' ), 'preview' => true, 'preview_strong' => true ),
				array( 'param_name' => 'subheadline', 'type' => 'textarea', 'heading' => esc_html__( 'Subheadline', 'renowise' ) ),
				array( 'param_name' => 'html_tag', 'type' => 'dropdown', 'heading' => esc_html__( 'HTML tag', 'renowise' ), 'preview' => true,
					'value' => array(
						esc_html__( 'h1', 'renowise' ) => 'h1',
						esc_html__( 'h2', 'renowise' ) => 'h2',
						esc_html__( 'h3', 'renowise' ) => 'h3',
						esc_html__( 'h4', 'renowise' ) => 'h4',
						esc_html__( 'h5', 'renowise' ) => 'h5',
						esc_html__( 'h6', 'renowise' ) => 'h6'
				) ),
				array( 'param_name' => 'size', 'type' => 'dropdown', 'heading' => esc_html__( 'Size', 'renowise' ), 'description' => 'Predefined heading sizes, independent of html tag',
					'value' => array(
						esc_html__( 'Inherit', 'renowise' ) 		=> 'inherit',
						esc_html__( 'Extra Small', 'renowise' ) 	=> 'extrasmall',
						esc_html__( 'Small', 'renowise' ) 		=> 'small',
						esc_html__( 'Medium', 'renowise' ) 		=> 'medium',
						esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
						esc_html__( 'Large', 'renowise' ) 		=> 'large',
						esc_html__( 'Extra large', 'renowise' ) 	=> 'extralarge',
						esc_html__( 'Huge', 'renowise' ) 		=> 'huge'
					)
				),				
				array( 'param_name' => 'align', 'type' => 'dropdown', 'heading' => esc_html__( 'Alignment', 'renowise' ),
					'value' => array(
						esc_html__( 'Inherit', 'renowise' ) 	=> 'inherit',
						esc_html__( 'Center', 'renowise' ) 	=> 'center',
						esc_html__( 'Left', 'renowise' ) 	=> 'left',
						esc_html__( 'Right', 'renowise' ) 	=> 'right'
					)
				),
				array( 'param_name' => 'dash', 'type' => 'dropdown', 'heading' => esc_html__( 'Dash', 'renowise' ), 'group' => esc_html__( 'Design', 'renowise' ),
					'value' => array(
						esc_html__( 'None', 'renowise' ) 			=> 'none',
						esc_html__( 'Top', 'renowise' ) 				=> 'top',
						esc_html__( 'Bottom', 'renowise' ) 			=> 'bottom',
						esc_html__( 'Top and bottom', 'renowise' ) 	=> 'top_bottom'
					)
				),
				array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Headline Style', 'renowise' ), 'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true, 
					'value' => array(
						esc_html__( 'Default', 'renowise' ) 	=> '',
						esc_html__( 'Square', 'renowise' ) 	=> 'square'
					)
				),
				array( 'param_name' => 'color_scheme', 'type' => 'dropdown', 'heading' => esc_html__( 'Color scheme', 'renowise' ), 'group' => esc_html__( 'Design', 'renowise' ), 'value' => $color_scheme_arr, 'preview' => true ),
				array( 'param_name' => 'superheadline_color', 'type' => 'colorpicker', 'heading' => esc_html__( 'Superheadline Color', 'renowise' ), 'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true ),
				array( 'param_name' => 'superheadline_bg_color', 'type' => 'colorpicker', 'heading' => esc_html__( 'Superheadline Background Color', 'renowise' ), 'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true ),
				array( 'param_name' => 'color', 'type' => 'colorpicker', 'heading' => esc_html__( 'Headline Color', 'renowise' ), 'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true ),
				array( 'param_name' => 'subheadline_color', 'type' => 'colorpicker', 'heading' => esc_html__( 'Subheadline Color', 'renowise' ), 'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true ),
				array( 'param_name' => 'supertitle_position', 'type' => 'checkbox', 'value' => array( esc_html__( 'Yes', 'renowise' ) => 'outside' ), 'heading' => esc_html__( 'Put supertitle outside H tag', 'renowise' ), 'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true ),
				array( 'param_name' => 'image', 'type' => 'attach_image', 'heading' => esc_html__( 'Background image', 'renowise' ), 'preview' => true, 'group' => esc_html__( 'Font', 'renowise' ) ),
				array( 'param_name' => 'font', 'type' => 'dropdown', 'heading' => esc_html__( 'Font', 'renowise' ), 'group' => esc_html__( 'Font', 'renowise' ), 'preview' => true,
					'value' => array ( esc_html__( 'Inherit', 'renowise' ) => 'inherit' ) + $font_arr
				),
				array( 'param_name' => 'font_subset', 'type' => 'textfield', 'heading' => esc_html__( 'Font subset', 'renowise' ), 'group' => esc_html__( 'Font', 'renowise' ), 'value' => 'latin,latin-ext', 'description' => 'E.g. latin,latin-ext,cyrillic,cyrillic-ext' ),
				array( 'param_name' => 'font_size', 'type' => 'textfield', 'heading' => esc_html__( 'Custom font size', 'renowise' ), 'group' => esc_html__( 'Font', 'renowise' ), 'description' => 'E.g. 20px or 1.5rem' ),
				array( 'param_name' => 'font_weight', 'type' => 'dropdown', 'heading' => esc_html__( 'Font weight', 'renowise' ), 'preview' => true, 'group' => esc_html__( 'Font', 'renowise' ),
					'value' => array(
						esc_html__( 'Default', 'renowise' ) 		=> '',
						esc_html__( 'Thin', 'renowise' ) 		=> 'thin',
						esc_html__( 'Lighter', 'renowise' ) 		=> 'lighter',
						esc_html__( 'Light', 'renowise' ) 		=> 'light',
						esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
						esc_html__( 'Medium', 'renowise' ) 		=> 'medium',
						esc_html__( 'Semi bold', 'renowise' ) 	=> 'semi-bold',
						esc_html__( 'Bold', 'renowise' ) 		=> 'bold',
						esc_html__( 'Bolder', 'renowise' ) 		=> 'bolder'
					)
				),
				array( 'param_name' => 'subheadline_font_weight', 'type' => 'dropdown', 'heading' => esc_html__( 'Subheadline Font weight', 'renowise' ), 'group' => esc_html__( 'Font', 'renowise' ),
					'value' => array(
						esc_html__( 'Default', 'renowise' ) 		=> '',
						esc_html__( 'Thin', 'renowise' ) 		=> 'thin',
						esc_html__( 'Lighter', 'renowise' ) 		=> 'lighter',
						esc_html__( 'Light', 'renowise' ) 		=> 'light',
						esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
						esc_html__( 'Medium', 'renowise' ) 		=> 'medium',
						esc_html__( 'Semi bold', 'renowise' ) 	=> 'semi-bold',
						esc_html__( 'Bold', 'renowise' ) 		=> 'bold',
						esc_html__( 'Bolder', 'renowise' ) 		=> 'bolder'
					)
				),
				array( 'param_name' => 'url', 'type' => 'textfield', 'heading' => esc_html__( 'URL', 'renowise' ), 'group' => esc_html__( 'URL', 'renowise' ) ),
				array( 'param_name' => 'target', 'type' => 'dropdown', 'heading' => esc_html__( 'Target', 'renowise' ), 'group' => esc_html__( 'URL', 'renowise' ),
					'value' => array(
						esc_html__( 'Self (open in same tab)', 'renowise' ) => '_self',
						esc_html__( 'Blank (open in new tab)', 'renowise' ) => '_blank'
					)
				)
			)
		) );
	}
}