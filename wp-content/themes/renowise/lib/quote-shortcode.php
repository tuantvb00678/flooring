<?php

function quote_caculator() {
  // step 1 pieces
  $step1_piece_acf = get_field('step1_piece', get_the_ID());
  if ( !empty($step1_piece_acf) ) {
    $step1_piece_id = get_product_by_acf($step1_piece_acf);
    $step1_piece = get_product_by_id($step1_piece_id);
  } else {
    $step1_piece = array(
      'product_id' => 4004,
      'post_title' => 'Piece',
      'price' => 100
    );
  }

  // step 2
  $brands = get_brand();
  $tag_color = get_product_tags('color');
  $tag_range = get_product_tags('range');

  // step 3
  $step3_product_acf = get_field('step3_products', get_the_ID());
  $edge_profiles_slug = 'edge-profiles';
  if ( !empty($step3_product_acf) ) {
    $edge_profiles_slug = $step3_product_acf->slug;
  }
  $edge_profiles = get_products_by_category_slug($edge_profiles_slug);

  // step 4
  $step4_product_acf = get_field('step4_products', get_the_ID());
  $extras_slug = 'extras';
  if ( !empty($step4_product_acf) ) {
    $extras_slug = $step4_product_acf->slug;
  }
  $extras = get_products_by_category_slug($extras_slug);

  // step 5
  $step5_product_acf = get_field('step5_products', get_the_ID());
  $cut_outs_slug = 'extras';
  if ( !empty($step5_product_acf) ) {
    $cut_outs_slug = $step5_product_acf->slug;
  }
  $cut_outs = get_products_by_category_slug($cut_outs_slug);

  // step 8

  ob_start(); ?>
    <div class="quote-caculator">
        <div class="quote-caculator__wrapper">
            <div class="quote-caculator__container">
                <div class="quote-caculator__inner">
                    <!-- multistep form -->
                    <div id="msform">
                        <!-- progressbar -->
                        <ul id="progressbar">
                            <li><?php _e('Layout', 'renowise'); ?></li>
                            <li><?php _e('Colour', 'renowise'); ?></li>
                            <li><?php _e('Edge profile', 'renowise'); ?></li>
                            <li><?php _e('Cut outs', 'renowise'); ?></li>
                            <li><?php _e('Extras', 'renowise'); ?></li>
                            <li><?php _e('Finalise Quote', 'renowise'); ?></li>
                        </ul>
                        <!-- fieldsets -->
                        <div class='step0' data-index="0">
                            <a class="js-start-quote" href="javascript:;">
                                <h2 class="fs-title"><?php _e('For Stone Quote - Click here', 'renowise'); ?></h2>
                                <img title="Quote Process" alt="Quote process" src="<?php echo get_template_directory_uri(); ?>/custom/img/quote_process.jpg" width="100%" height="auto">
                            </a>
                        </div>
                        <div class='step step1' data-index="1">
                            <h2 class="fs-title"><?php _e('Layout', 'renowise'); ?></h2>
                            <div class="step-content">
                                <div class="step-image">
                                    <img title="Quote Process" alt="Quote process" src="<?php echo get_template_directory_uri(); ?>/custom/img/quote_process.jpg" width="100%" height="auto">
                                </div>
                                <div class="step-info">
                                    <h5><?php _e('1. Select how many pieces in your kitchen or bathrooms:', 'renowise'); ?></h5>
                                    <div class="quote-item">
                                        <label><?php _e('Pieces', 'renowise'); ?></label>
                                        <input type="text" name="pieces" class="js-step1-pieces" value="1" data-id="<?php echo $step1_piece['product_id']; ?>" data-price="<?php echo $step1_piece['price']; ?>" data-title="<?php echo $step1_piece['post_title']; ?>"/>
                                    </div>
                                    <h5><?php _e('2. Enter your measurements below', 'renowise'); ?></h5>
                                    <div class="quote-table js-step1-table">
                                        <div class="quote-group js-step1-row">
                                            <div class="quote-item">
                                                <label><?php _e('Width (mm):', 'renowise'); ?></label>
                                                <input type="text" name="width" class="js-step1-width" value="1"/>
                                            </div>
                                            <div class="quote-item">
                                                <label><?php _e('Length (mm):', 'renowise'); ?></label>
                                                <input type="text" name="length" class="js-step1-length" value="1"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="button" name="next" class="next action-button" value="Next" />
                        </div>
                        <div class='step step2' data-index="2">
                            <div class="quote-overlay js-overlay"></div>
                            <div class="spinner js-spinner">
                                <div class="rect1"></div>
                                <div class="rect2"></div>
                                <div class="rect3"></div>
                                <div class="rect4"></div>
                                <div class="rect5"></div>
                            </div>

                            <h2 class="fs-title"><?php _e('Colour', 'renowise'); ?></h2>
                            <h3 class="fs-subtitle"><?php _e('Select the range you would like to choose your benchtop colour from: To see our full range of colours, please', 'renowise'); ?>
                                <a href="#" class="js-step2-search-link" alt="Show all group brand categories"><?php _e('Click Here', 'renowise'); ?></a>.</h3>


                            <div class="step-content step-content--brands js-step2-content-form">
                                <h5><?php _e('1) Search by Name', 'renowise'); ?></h5>
                                <div class="quote-item search-form">
                                    <input type="text" name="name" placeholder="Search" class="js-step2-search-input"/>
                                    <input type="button" name="search" class="search action-button js-step2-search-button" value="Search " />
                                </div>
                                <?php if ( !empty($brands) ) : ?>
                                <h5><?php _e('2) Select by Brand', 'renowise'); ?></h5>
                                <div class="search-grid search-grid--brands js-ajax-brand">
                                    <?php foreach ($brands as $brand) : ?>
                                    <?php if ($brand['option'] === 'brand'): ?>
                                    <div class="search-item search-item--brand js-ajax-brand-item" data-id="<?php echo $brand['term_id']; ?>" data-name="<?php echo $brand['name']; ?>" data-slug="<?php echo $brand['slug']; ?>">
                                        <div class="search-item__inner">
                                            <div class="search-image">
                                                <img src="<?php echo $brand['image']?>" alt="<?php echo $brand['name']?>">
                                                <h6><?php echo $brand['name']; ?></h6>
                                            </div>
                                        </div>
                                    </div>
                                      <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>

                                <h5><?php _e('3) Select by Color', 'renowise'); ?></h5>
                                <?php if ( !empty($tag_color) ) : ?>
                                <div class="search-grid search-grid--color js-ajax-color">
                                    <?php foreach ($tag_color as $color) : ?>
                                    <div class="search-item search-item--color js-ajax-color-item" data-name="<?php echo $color['name']; ?>" data-color="<?php echo $color['slug']; ?>">
                                        <div class="search-item__inner">
                                            <div class="search-image">
                                                <img src="<?php echo $color['image']; ?>" alt="<?php echo $color['name']; ?>">
                                                <h6><?php echo $color['name']; ?></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>

                                <?php if ( !empty($tag_range) ) : ?>
                                <h5><?php _e('4) Select by Price Range', 'renowise'); ?></h5>
                                <div class="search-grid search-grid--range js-ajax-range">
                                    <?php foreach ($tag_range as $range) : ?>
                                    <div class="search-item search-item--range js-ajax-range-item" data-name="<?php echo $range['name']; ?>" data-range="<?php echo $range['slug']; ?>">
                                        <div class="search-item__inner">
                                            <div class="search-image">
                                                <img src="<?php echo $range['image']?>" alt="<?php echo $range['name']; ?>">
                                                <h6><?php echo $range['name']; ?></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                            </div>

                            <div class="step-content--step2 js-ajax-content">
                                <h2 class="fs-title disabled">Search by: <span class="js-search-ajax-title">Name</span></h2>
                                <p class="fs content js-search-ajax-no-results disabled"><?php _e('No results.', 'renowise'); ?></p>
                                <!-- Ajax content show at here-->
                                <div class="search-ajax__wrapper js-search-ajax-content">
                                </div>
                            </div>

                            <input type="button" name="previous" class="previous action-button js-step2-prev" value="Previous" />
                            <input type="button" name="next" class="next action-button" value="Next" />
                        </div>
                        <div class='step step3' data-index="3">
                            <h2 class="fs-title"><?php _e('Select the preferred thickness of your benchtop edge:', 'renowise'); ?></h2>
                            <div class="step-content">
                                <?php if (!empty($edge_profiles)) : ?>
                                <div class="edge-grid js-edge-grid" data-id="0" data-title="" data-price="0" data-image="">
                                    <?php foreach($edge_profiles as $edge) : ?>
                                    <?php
                                      $path = get_template_directory_uri() . "/custom/img/";
                                      $edg_thumb = (strpos($edge['post_title'], '40mm') !== false) ? $path.'edge_40mm.jpg' : $path.'edge_20mm.jpg';
                                      $edge_image = get_the_post_thumbnail_url($edge['product_id']);
                                      if (empty($edge_image)) {
                                        $edge_image = get_default_image();
                                      }
                                    ?>
                                    <div class="edge-item" data-id="<?php echo $edge['product_id'];?>" data-title="<?php echo $edge['post_title']; ?>" data-price="<?php echo $edge['price'];?>" data-image="<?php echo $edg_thumb; ?>">
                                        <div class="edge-item__inner">
                                            <div class="edge-image">
                                                <img src="<?php echo $edge_image; ?>" alt="<?php echo $edge['post_title']; ?>">
                                            </div>
                                            <div class="edge-thumb">
                                                <img src="<?php echo $edg_thumb; ?>" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                            <input type="button" name="next" class="next action-button" value="Next" />
                        </div>
                        <div class='step step4' data-index="4">
                            <h2 class="fs-title"><?php _e('Select any cutouts you require for your benchtop', 'renowise'); ?></h2>
                            <div class="step-content">
                                <?php if (!empty($cut_outs)) : ?>
                                    <div class="edge-grid js-cutouts-inputs">
                                        <?php foreach($cut_outs as $cutout) : ?>
                                        <?php
                                            $cutout_image = get_the_post_thumbnail_url($cutout['product_id']);
                                            if (empty($cutout_image)) {
                                                $cutout_image = get_default_image();
                                            }
                                        ?>
                                            <div class="edge-item">
                                                <div class="edge-item__inner">
                                                    <div class="edge-image">
                                                        <img src="<?php echo $cutout_image; ?>" alt="<?php echo $cutout['post_title']; ?>" width="240" height="181">
                                                    </div>
                                                    <div class="edge-content">
                                                        <h6><?php echo $cutout['post_title']; ?></h6>
                                                        <input type="text" name="qty" class="js-cutout-input" value="1" data-id="<?php echo $cutout['product_id']; ?>" data-title="<?php echo $cutout['post_title']; ?>" data-price="<?php echo $cutout['price']; ?>" data-image="<?php echo $cutout_image; ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                            <input type="button" name="next" class="next action-button" value="Next" />
                        </div>
                        <div class='step step5' data-index="5">
                            <h2 class="fs-title"><?php _e('Select any extra features you would like in your stone benchtop', 'renowise'); ?></h2>
                            <div class="step-content">
                                <?php if (!empty($extras)) : ?>
                                    <div class="edge-grid js-extras-input">
                                        <?php foreach($extras as $extra) : ?>
                                            <?php
                                                $extra_image = get_the_post_thumbnail_url($extra['product_id']);
                                                if (empty($extra_image)) {
                                                    $extra_image = get_default_image();
                                                }
                                            ?>
                                            <div class="edge-item">
                                                <div class="edge-item__inner">
                                                    <div class="edge-image">
                                                        <img src="<?php echo $extra_image; ?>" alt="<?php echo $extra['post_title']; ?>" width="240" height="181">
                                                    </div>
                                                    <div class="edge-content">
                                                        <h6><?php echo $extra['post_title']; ?></h6>
                                                        <input type="text" name="qty" class="js-extra-input" value="1" data-id="<?php echo $extra['product_id']; ?>" data-title="<?php echo $extra['post_title']; ?>" data-price="<?php echo $extra['price']; ?>" data-image="<?php echo $extra_image; ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                            <input type="button" name="next" class="next action-button js-step6-next-button active" value="Next" />
                        </div>
                        <div class='step step6' data-index="6">
                            <h2 class="fs-title"><?php _e('Finalise Quote', 'renowise'); ?></h2>
                            <div class="step-content">
                                <div class="edge-grid edge-grid--final">
                                <div class="edge-item">
                                    <div class="edge-item__inner">
                                        <div class="edge-content">
                                            <h5><?php _e('Measurements', 'renowise'); ?></h5>
                                            <h6 class="js-final-piece-not-found disabled"><?php _e('No pieces Found', 'renowise'); ?></h6>
                                        </div>
                                        <div class="edge-option js-final-piece-option"></div>
                                    </div>
                                </div>
                                <div class="edge-item js-final-edge-thickness">
                                    <div class="edge-item__inner">
                                        <div class="edge-content">
                                            <h5 class="js-final-edge-title"><?php _e('EDGE THICKNESS:', 'renowise'); ?></h5>
                                        </div>
                                        <div class="edge-thumb">
                                            <img class="js-final-edge-thumb" src="<?php echo get_template_directory_uri(); ?>/custom/img/edge_40mm.jpg" width="240" height="181">
                                        </div>
                                    </div>
                                </div>
                                <div class="edge-item js-final-colour-item">
                                    <div class="edge-item__inner">
                                        <div class="edge-content">
                                            <h5><?php _e('STONE COLOURS:', 'renowise'); ?> <span class="js-final-colour-title"><?php _e('Coral Reef', 'renowise'); ?></span></h5>
                                        </div>
                                        <div class="edge-image">
                                            <img class="js-final-colour-img" src="<?php echo get_template_directory_uri(); ?>/custom/img/edge_40mm.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="edge-item">
                                    <div class="edge-item__inner">
                                        <div class="edge-content">
                                            <h5>EXTRAS</h5>
                                            <h6 class="js-final-extras-not-found disabled"><?php _e('No Extras Found', 'renowise'); ?></h6>
                                        </div>
                                        <div class="edge-option js-final-extras">
                                        </div>
                                    </div>
                                </div>
                                <div class="edge-item">
                                    <div class="edge-item__inner">
                                        <div class="edge-content">
                                            <h5>CUTOUTS</h5>
                                            <h6 class="js-final-cutouts-not-found disabled"><?php _e('No Cutouts Found', 'renowise'); ?></h6>
                                        </div>
                                        <div class="edge-option js-final-cutouts">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="quote-total__label disabled">
                                <span class="quote-total__text js-quote-total-text"><?php _e('Total', 'renowise'); ?></span>
                                <span class="quote-total__currency js-quote-total-currency">$</span>
                                <span class="quote-total__price js-quote-total-price">0.00</span>
                            </div>
                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                            <input type="button" name="next" class="next action-button" value="Submit" />
                        </div>
                        <div class='step step7' data-index="7">
                            <div class="quote-overlay js-overlay-step7"></div>
                            <div class="spinner js-spinner-step7">
                                <div class="rect1"></div>
                                <div class="rect2"></div>
                                <div class="rect3"></div>
                                <div class="rect4"></div>
                                <div class="rect5"></div>
                            </div>
                            <h2 class="fs-title"><?php _e('Quote Enquiries', 'renowise'); ?></h2>
                            <div class="contact">
                                <div class="contact-group">
                                    <label for="contact_name"><?php _e('Name', 'renowise'); ?></label>
                                    <input class="js-contact-name" type="text" name="contact_name" placeholder="<?php _e('Name', 'renowise'); ?>"/>
                                </div>
                                <div class="contact-group">
                                    <label for="contact_email"><?php _e('Email', 'renowise'); ?></label>
                                    <input class="js-contact-email" type="text" name="contact_email" placeholder="<?php _e('Email', 'renowise'); ?>"/>
                                </div>
                                <div class="contact-group">
                                    <label for="contact_phone"><?php _e('Phone', 'renowise'); ?></label>
                                    <input class="js-contact-phone" type="text" name="contact_phone" placeholder="<?php _e('Phone', 'renowise'); ?>"/>
                                </div>
                                <div class="contact-group">
                                    <label for="contact_address"><?php _e('Project Address', 'renowise'); ?></label>
                                    <input class="js-contact-address" type="text" name="contact_address" placeholder="<?php _e('Project Address', 'renowise'); ?>"/>
                                </div>
                                <div class="contact-group">
                                    <label for="contact_project_begin"><?php _e('Project Status', 'renowise'); ?></label>
                                    <select class="js-contact-project-begin" name="contact_project_begin" id="contact_project_begin" class="q-proc-drop">
                                        <option value=""><?php _e('Please Select...', 'renowise'); ?></option>
                                        <option value="<?php _e('Just asking', 'renowise'); ?>"><?php _e('Just asking', 'renowise'); ?></option>
                                        <option value="<?php _e('Starting to renovate / build', 'renowise'); ?>"><?php _e('Starting to renovate / build', 'renowise'); ?></option>
                                        <option value="<?php _e('Ready for Benchtops', 'renowise'); ?>"><?php _e('Ready for Benchtops', 'renowise'); ?></option>
                                    </select>
                                </div>
                                <div class="contact-group">
                                    <label for="contact_found_you_via"><?php _e('I found you via', 'renowise'); ?></label>
                                    <select class="js-contact-found-you-via" name="contact_found_you_via" id="contact_found_you_via" class="q-proc-drop">
                                        <option value=""><?php _e('Please Select...', 'renowise'); ?></option>
                                        <option value="<?php _e('Google Search', 'renowise'); ?>"><?php _e('Google Search', 'renowise'); ?></option>
                                        <option value="<?php _e('Houzz', 'renowise'); ?>"><?php _e('Houzz', 'renowise'); ?></option>
                                        <option value="<?php _e('Hi Pages', 'renowise'); ?>"><?php _e('Hi Pages', 'renowise'); ?></option>
                                        <option value="<?php _e('Newspaper', 'renowise'); ?>"><?php _e('Newspaper', 'renowise'); ?></option>
                                        <option value="<?php _e('Cabinetmaker / Builder', 'renowise'); ?>"><?php _e('Cabinetmaker / Builder', 'renowise'); ?></option>
                                        <option value="<?php _e('Referral /Word of Mouth', 'renowise'); ?>"><?php _e('Referral /Word of Mouth', 'renowise'); ?></option>
                                        <option value="<?php _e('Business Card', 'renowise'); ?>"><?php _e('Business Card', 'renowise'); ?></option>
                                        <option value="<?php _e('Social Media', 'renowise'); ?>"><?php _e('Social Media', 'renowise'); ?></option>
                                        <option value="<?php _e('Flyer', 'renowise'); ?>"><?php _e('Flyer', 'renowise'); ?></option>
                                        <option value="<?php _e('Home Show', 'renowise'); ?>"><?php _e('Home Show', 'renowise'); ?></option>
                                    </select>
                                </div>
                            </div>
                            <?php
                                $subject_text = get_field('quote_enquiries_subject_mail', get_the_ID());
                                $body_text = get_field('quote_enquiries_mail', get_the_ID());
                                $subject = base64_encode( $subject_text );
                                $body = base64_encode( $body_text );
                            ?>
                            <p class="step7-msg-success disabled"><?php _e('Send mail success.'); ?></p>
                            <input type="hidden" class="js-contact-subject" value="<?php echo $subject; ?>"/>
                            <input type="hidden" class="js-contact-body" value="<?php echo $body; ?>"/>
                            <input type="button" name="previous" class="previous action-button" value="<?php _e('Previous', 'renowise'); ?>" />
                            <input type="button" name="previous" class="next action-button js-step7-next-button disabled" value="<?php _e('Next', 'renowise'); ?>" />
                            <input type="button" name="submit" class="submit action-button js-quote-submit-button" value="<?php _e('Get A Quote', 'renowise'); ?>" data-total="0"/>
                        </div>

                        <div class='step step8' data-index="8">
                            <div class="step8__inner">
                                <div class="step8__blocks">
                                    <div class="step8__block">
                                        <p class="step8__price">
                                            <span class="step8__price-text"><?php _e('Estimated Price', 'renowise'); ?></span>
                                            <span class="step8__price-num js-step8-price-num"><span>$</span><?php _e('880.00', 'renowise'); ?></span>
                                        </p>
                                        <div class="step8__p">
                                            <p><?php _e('This calculator is designed to provide an estimate only and is based on the measurements that you have entered. For a complete onsite quote click the button below.', 'renowise'); ?></p>
                                            <p><?php _e('This price includes supply, manufacture, installation and is inclusive of GST. Additional charges may apply to selected areas. To find out if these charges apply to you,
please call us on 03 9794 6811.', 'renowise'); ?></p>
                                        </div>
                                    </div>
                                    <div class="step8__info">
                                        <p class="step8__info-title">
                                          <?php _e('Is this within your budget?', 'renowise'); ?>
                                        </p>
                                        <p class="step8__info-content">
                                          <?php _e('The color and sizes you have selected determines your final estimate. Below is the estimated price difference between what you have chosen and other stone ranges that we also have available.', 'renowise'); ?>
                                        </p>
                                    </div>
                                    <div class="step8__content">
                                        <p class="step8__content-title">
                                            You have selected a <span class="step8__content-select js-step8-content-select">QUANTUM QUARTZ Q1</span> Colour. If you wish to change your colour selection to an alternate group, please see below for relevant pricing.
                                        </p>
                                        <div class="step8__content-list">
                                            <?php if (!empty($brands)) : ?>
                                            <div class="step8__brands">
                                                <?php
                                                    foreach ($brands as $brand) :
                                                        $child = get_brand($brand['term_id']);
                                                        if (!empty($child)):
                                                ?>
                                                <div class="step8__brand">
                                                    <p class="step8__brand-parent"><?php echo $brand['name']; ?></p>
                                                    <?php foreach ($child as $item) : ?>
                                                    <p class="step8__brand-child">
                                                        <span class="step8__brand-child-des"><?php echo $item['des']; ?> </span>
                                                        <span class="step8__brand-child-name"><?php echo $item['name']; ?></span>
                                                        <span class="step8__brand-child-selected js-step8-selected" data-id="<?php echo $item['term_id']; ?>">
                                                            <span class="js-step8-text"> = add $0.00</span>
                                                            <span class="js-step8-label disabled">Selected</span>
                                                        </span>
                                                    </p>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                            <?php endif; ?>
                                            <p class="step8__content-back-step2">
                                                <a href="#"><?php _e('Click here to pick another colour', 'renowise'); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="step8__image">
                                    <div class="step8__image-wrapper">
                                        <img src="<?php echo get_template_directory_uri(); ?>/custom/img/quote_final.jpg" width="464" height="336">
                                    </div>
                                    <div class="step8__buttons">
                                        <div class="step8__button-request">
                                            <a href="onsite-quote.php" title="<?php _e('REQUEST AN ONSITE QUOTE', 'renowise'); ?>"><?php _e('REQUEST AN ONSITE QUOTE', 'renowise'); ?></a>
                                        </div>
                                        <div class="step8__button-pdf">
                                            <a class="js-step8-link-pdf" href="#" target="_blank" title="<?php _e('Download pdf of Quote', 'renowise'); ?>"><?php _e('Download pdf of Quote', 'renowise'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
  <?php
}

add_shortcode( 'quote_caculator', 'quote_caculator' );
?>
