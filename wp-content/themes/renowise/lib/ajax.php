<?php

/*

1. Layout

	- 1. Select how many pieces in your kitchen or bathrooms: <number>
	- 2. Enter your measurements below
		- PIECE 01:  Length - <number> - mm | Width - <number> mm


2. Colour
	- 1. Filter Select by Brand ( category của đá )
	- 2. Filter Select by Price Range
	- 3. Select đá theo màu ( là product đã có giá tiền riêng theo màu  )


3. Edge Profile
	- Select the preferred thickness of your benchtop edge: ( phần này là chọn độ dày của bề mặt)
	- Mình sẽ tạo 1 category trong woo là độ dày
	- Và tạo các sản phẩm, có độ dày (20mm , 40mm) có giá + hình ảnh

4. Cut outs
	- Select any cutouts you require for your benchtop
	- Phần này cũng tương tự như phần độ dày sẽ có : catalog về cut-outs
	- Gồm các product (image + giá)
		+ TOPMOUNT SINK - $66
		+ HOT PLATE CUT OUT - $66
		+ UNDERMOUNT SINK - $220

5. Extras
	- Select any extra features you would like in your stone benchtop :
	- phần này cũng nên là danh mục Extra và các product tương ứng tạo bên trong wooo
		+ DRAINER GROOVES SET OF 5 - $550
		+ DRILL HOLES - $35

6. Finalise Quote
	- Bước này sẽ show ra chi tiết từ bước 1 đã được chọn


*/


/**
 * Register ajax redirection script
 */
function register_ajax_scripts() {
  // Enqueue script
  wp_register_script(
    'quote_script',
    esc_url( get_template_directory_uri() . '/custom/custom.js' ),
    array('jquery'),
    null,
    false
  );
  wp_enqueue_script('quote_script');

  wp_localize_script(
    'quote_script',
    'quote_vars',
    array(
      'ajax_url' => admin_url( 'admin-ajax.php' ),
    )
  );
}
add_action('wp_enqueue_scripts','register_ajax_scripts');

function generate_pdf($user, $product, $curency) {
  $obj = new GeneratePDF();
  $pdf_name = sprintf('quote-%s-%s', date('Y-m-d', time()), strtotime("now"));

  $obj->write($user['total_price'], 'price');
  $obj->write($user['contact_name'], 'name');
  $obj->write($user['contact_email'], 'email');
  $obj->write($user['contact_phone'], 'phone');

  $obj->write($user['stone_color'], 'brand_color');
  $obj->write($user['edge_data'], 'edge_work');

  if (!empty($product['cut_outs_data'])) {
    $y = 142;
    foreach ($product['cut_outs_data'] as $item) {
      $y += 5;
      $cut_outs = $item['title'] .': ' . $curency. $item['price'] . ' x ' . $item['qty'] . '(qty)';
      $obj->write($cut_outs, 'cut_outs', $y);
    }
  }

  if (!empty($product['extras_data'])) {
    $e_y = 162;
    foreach ($product['extras_data'] as $item) {
      $e_y += 5;
      $extras = $item['title'] .': ' . $curency. $item['price'] . ' x ' . $item['qty'] . '(qty)';
      $obj->write($extras, 'cut_outs', $e_y);
    }
  }

  return $obj->export($pdf_name, 1);
}

// AJAX SEND MAIL
function quote_contact_form() {
  $curency = '$';

  $message = array();
  $message['error'] = '';

  if (empty($_POST['json'])) {
    $message['error'] = __('Error: Data not found', 'renowise');
  }

  if (empty($_POST['total_price'])) {
    $message['error'] = __('Error: Total Price not found', 'renowise');
  }

  if ( empty($message['error']) ) {
    // Data info user
    $user = array();

    $user['contact_name'] = $_POST['contact_name'];
    $user['contact_email'] = $_POST['contact_email'];
    $user['contact_phone'] = $_POST['contact_phone'];
    $user['contact_address'] = $_POST['contact_address'];
    $user['contact_project_begin'] = $_POST['contact_project_begin'];
    $user['contact_found_you_via'] = $_POST['contact_found_you_via'];
    $user['total_price'] = $curency . $_POST['total_price'];

    // Data send mail
    $jsonData = stripslashes($_POST['json']);
    $json = json_decode($jsonData, true);

    $pieceData = $json['pieceData'];
    $pieces_price = $curency . $pieceData;

    $pieceOptionData = $json['pieceOptionData'];
    if ( !empty($pieceOptionData) ) {
      $pieceOptionHtml = '';
      foreach ($pieceOptionData as $pieceOption) {
        $pieceOptionHtml .= $pieceOption['index'] . '. Piece: ' . 'width: ' . $pieceOption['width'] . 'mm, Length: ' . $pieceOption['length'] . 'mm.<br>';
      }
      $user['pieces_option'] = $pieceOptionHtml;
    }

    // color data
    $colorData = $json['colorData'];
    $colorHtml = '';
    if ( !empty($colorData) && !empty($colorData['title'])) {
      $colorHtml = $colorData['title'] .': ' . $curency. $colorData['price'] . ' x ' . $colorData['qty'] . '(qty)';
    }
    $user['stone_color'] = $colorHtml;

    // edgeData
    $edgeData = $json['edgeData'];
    $edgeHtml = '';
    if ( !empty($edgeData) && !empty($edgeData['price']) && !empty($edgeData['title'])) {
      $edgeHtml = $edgeData['title'] .': ' . $curency. $edgeData['price'] . ' x ' . $edgeData['qty'] . '(qty)';
      $user['edge_data'] = $edgeHtml;
    }
    $user['edge_data'] = $edgeHtml;

    // cutOutsData
    $cutOutsData = $json['cutOutsData'];
    $cutOutHtml = '';
    if ( !empty($cutOutsData) ) {
      foreach($cutOutsData as $cutOut) {
        $cutOutHtml .= $cutOut['title'] .': ' . $curency. $cutOut['price'] . ' x ' . $cutOut['qty'] . '(qty)<br>';
      }
    }
    $product['cut_outs_data'] = $cutOutsData;
    $user['cut_outs_data'] = $cutOutHtml;

    // extrasData
    $extrasData = $json['extrasData'];
    $extrasHtml = '';
    if ( !empty($extrasData) ) {
      foreach($extrasData as $extra) {
        $extrasHtml .= $extra['title'] .': ' . $curency. $extra['price'] . ' x ' . $extra['qty'] . '(qty)<br>';
      }
    }
    $product['extras_data'] = $extrasData;
    $user['extras_data'] = $extrasHtml;

    $subject = '';
    $body = '';
    if (!empty($_POST['contact_subject'])) {
      $subject = base64_decode($_POST['contact_subject']);
    }
    if (!empty($_POST['contact_body'])) {
      $body = base64_decode($_POST['contact_body']);
    }

    quote_send_mail($user, $subject, $body);

    $message['pdf'] = generate_pdf($user, $product, $curency);

    $message['success'] = __('Success: Submit contact form success.', 'renowise');
  }

  echo json_encode($message);
  header('Content-Type: application/json; charset=UTF-8');
  wp_die();
}
add_action( 'wp_ajax_quote_contact_form', 'quote_contact_form');
add_action( 'wp_ajax_nopriv_quote_contact_form', 'quote_contact_form' );



// AJAX BRAND SEARCH CHILD ITEM
function quote_ajax_brand_search_child() {
  $message = array();
  $message['error'] = '';
  $id = '';
  if (!empty($_POST['id'])) {
    $id = $_POST['id'];
  }

  $group_name = '';
  if (!empty($_POST['name'])) {
    $group_name = $_POST['name'];
  }

  if ( empty($message['error']) ) {
    ob_start();

    $term_ids = query_product_cat_child($id);
    $brands = get_product_cat_child($term_ids);
    include(locate_template('custom/brands.php'));

    $data = ob_get_clean();
    $message['data'] = $data;
    $message['success'] = __('Success: Search brand child success.', 'renowise');
  }
  echo json_encode($message);
  header('Content-Type: application/json; charset=UTF-8');
  wp_die();
}
add_action( 'wp_ajax_quote_ajax_brand_search_child', 'quote_ajax_brand_search_child');
add_action( 'wp_ajax_nopriv_quote_ajax_brand_search_child', 'quote_ajax_brand_search_child' );

// AJAX PRODUCT SEARCH
function quote_ajax_products_search() {
  $message = array();
  $message['error'] = '';
  $search = '';
  $slug = '';
  $type = '';

  if (!empty($_POST['search'])) {
    $search = $_POST['search'];
  }

  if (!empty($_POST['type'])) {
    $type = $_POST['type'];
  }

  if (!empty($_POST['slug'])) {
    $slug = $_POST['slug'];
  }

  if ( empty($message['error']) ) {
    ob_start();
    switch ($type) {
      case 'brand':
        // AJAX RANGE SEARCH
        $products = get_products_by_category_slug($slug, false, $search, 'product_cat');
        break;
      case 'color':
        // AJAX COLOR SEARCH
        $products = get_products_by_category_slug($slug, false, $search, 'product_tag');
        break;
      case 'range':
        // AJAX RANGE SEARCH
        $products = get_products_by_category_slug($slug, false, $search, 'product_tag');
        break;
      default:
        // AJAX RANGE SEARCH
        $products = get_products_by_category_slug('', true, $search, 'product_cat');
        break;
    }
    include(locate_template('custom/products.php'));
    $data = ob_get_clean();
    $message['data'] = $data;
    $message['success'] = __('Success: Search products success.', 'renowise');
  }

  echo json_encode($message);
  header('Content-Type: application/json; charset=UTF-8');
  wp_die();
}

add_action( 'wp_ajax_quote_ajax_products_search', 'quote_ajax_products_search');
add_action( 'wp_ajax_nopriv_quote_ajax_products_search', 'quote_ajax_products_search' );

// ADD MULTIPLE ID PRODUCTS AJAX WOOCOMMERCE

add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');

function woocommerce_ajax_add_to_cart() {
  $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
  $quantity = 1;
  $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
  $product_status = get_post_status($product_id);

  if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity) && 'publish' === $product_status) {

    do_action('woocommerce_ajax_added_to_cart', $product_id);

    if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
      wc_add_to_cart_message(array($product_id => $quantity), true);
    }

    WC_AJAX :: get_refreshed_fragments();
  } else {

    $data = array(
      'error' => true,
      'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

    echo wp_send_json($data);
  }

  wp_die();
}
