<?php
function calacatta_superior($atts = [], $content = null, $tag = '') {

  // normalize attribute keys, lowercase
  $atts = array_change_key_case((array)$atts, CASE_LOWER);

  $products = array();
  $brands = array();

  if ( !empty($atts['slug']) ) {
    $slug_param = $atts['slug'];

    $term = get_id_brand_by_slug($slug_param);
    if ( !empty($term) ) {
      $slug = $term->slug;
      $term_id = $term->term_id;

      $brands = get_brand($term_id);

      $term_child = query_product_cat_child($term_id);
      $term_child[] = $term_id;

      if (!empty($term_child)) {
        $term_ids = $term_child;
      } else {
        $term_ids = $slug;
      }

      $products = get_products_by_category_slug($term_ids, false, '', 'product_cat');
    }
  } else {
    $brands = get_brand(0);
    $term_ids = array();
    if (!empty($brands)) {
      foreach ($brands as $brand) {
        if ($brand['option'] === 'brand') {
          $term_ids[] = $brand['term_id'];
        }
      }
      $products = get_products_by_category_slug($term_ids, false, '', 'product_cat');
    }
  }

  // check is archive category product
  /*
    if ( is_product_category() ) {
      $cat = get_queried_object();
      $parent_id = $cat->term_id;
      $brands = get_brand($parent_id);

      $term_child = query_product_cat_child($parent_id);
      if ( !empty($term_child) ) {
        $term_ids = $term_child;
      } else {
        $term_ids = $cat->slug;
      }
      $products = get_products_by_category_slug($term_ids, false, '', 'product_cat');
    }
  */


  ob_start(); ?>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='https://npmcdn.com/isotope-layout@3/dist/isotope.pkgd.js'></script>
    <script src='https://npmcdn.com/imagesloaded@4/imagesloaded.pkgd.js'></script>
    <script>
      jQuery(document).ready(function($){
        // init Isotope
        var $grid = $('.gallery__grid-inner').isotope({
          itemSelector: '.gallery__grid-item',
          layoutMode: 'fitRows'
        });
        // filter functions
        var filterFns = {
          // show if number is greater than 50
          numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt( number, 10 ) > 50;
          },
          // show if name ends with -ium
          ium: function() {
            var name = $(this).find('.name').text();
            return name.match( /ium$/ );
          }
        };
        // bind filter button click
        $('.js-filters-button-group').on( 'click', '.js-button-group', function() {
          var filterValue = $( this ).attr('data-filter');
          // use filterFn if matches value
          filterValue = filterFns[ filterValue ] || filterValue;
          $grid.isotope({ filter: filterValue });
        });
        // change is-checked class on buttons
        $('.js-button-group').each( function() {
          var $buttonGroup = $( this );
          $buttonGroup.on( 'click', function() {
            $('.js-filters-button-group').find('.js-button-group').removeClass('is-checked');
            $( this ).addClass('is-checked');
          });
        });

        // init Masonry
        var $grid = $('.gallery__grid-inner').masonry({
          itemSelector: '.gallery__grid-item',
          percentPosition: true,
          columnWidth: '.gallery__grid-item'
        });
        // layout Masonry after each image loads
        $grid.imagesLoaded().progress( function() {
          $grid.masonry();
        });

        $('.js-gallery-grid-item').click(function(e) {
          var title = $(this).attr('data-title');
          var image = $(this).attr('data-image');

          var holder = $('#post-single');
          holder.removeClass('disabled');
          holder.find('#single-post-title').html(title);
          holder.find('#single-post-image').attr('src', image);

          $('html, body').animate({
            scrollTop: $('#single-buffer').offset().top - 150
          }, 500);
        });
      });
    </script>
    <div id="single-buffer"></div>
    <section class="section-gallery">
        <div class="gallery__wrapper">
            <div class="gallery__containner">
                <div class="gallery__inner">
                    <div class="gallery__post-single disabled" id="post-single">
                        <div class="gallery__post-content" id="single-post-content">
                            <p>
                                <img id="single-post-image" src="" width="1100" height="550">
                            </p>
                        </div>
                        <h5 class="gallery__post-title" id="single-post-title">
                          <?php _e('Calacatta Superior', 'renowise'); ?>
                        </h5>
                    </div>

                    <?php if (!empty($brands)) : ?>
                    <div class="gallery__filters">
                        <div id="options" class="gallery__filters-grid">
                            <div id="filters" class="js-filters-button-group gallery__filters-grid-inner option-set clearfix" data-option-key="filter">
                                <span class="gallery__filters-item"><a class="js-button-group" href="javascript:void(0)" data-filter="*" class=""><?php _e('ALL', 'renowise'); ?></a></span>
                                    <?php if ($brands && empty($atts['slug'])) : ?>
                                    <?php foreach($brands as $brand) : ?>
                                        <?php if ($brand['option'] === 'brand'): ?>
                                        <span class="gallery__filters-item"><a class="js-button-group" href="javascript:void(0)" data-filter=".<?php echo $brand['slug']; ?>" class="selected"><?php echo $brand['name']; ?></a></span>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php endif; ?>

                                  <?php if ($brands && !empty($atts['slug'])) : ?>
                                    <?php foreach($brands as $brand) : ?>
                                      <span class="gallery__filters-item"><a class="js-button-group" href="javascript:void(0)" data-filter=".<?php echo $brand['slug']; ?>" class="selected"><?php echo $brand['name']; ?></a></span>
                                    <?php endforeach; ?>
                                  <?php endif; ?>
                            </div>
                        </div> <!-- #options -->
                    </div>
                    <?php endif; ?>

                    <?php if ( !empty($products) ) : ?>
                    <div class="gallery__grid isotope">
                        <div class="gallery__grid-container">
                            <div class="gallery__grid-inner">
                                <?php foreach($products as $product): ?>
                                <?php
                                  $term_slugs = get_categories_by_product_id($product['product_id'], $term_ids, true);
                                  $term_names = get_categories_by_product_id($product['product_id'], $term_ids, false);
                                  if (!empty($term_slugs)) {
                                    $term_slug = implode(' ', $term_slugs);
                                  } else {
                                    $term_slug = $product['term_slug'];
                                  }

                                  if (!empty($term_names)) {
                                    $term_name = implode(' ', $term_names);
                                  } else {
                                    $term_name = $product['term_name'];
                                  }

                                  $image = get_the_post_thumbnail_url($product['product_id'], 'full');
                                  if (empty($image)) {
                                    $image = get_default_image();
                                  }

                                  $url = get_permalink($product['product_id']);

                                ?>
                                <div class="gallery__grid-item element <?php echo $term_slug; ?>">
                                    <div class="gallery__grid-item-inner">
                                        <div class="gallery__grid-item-thumbnail">
                                            <a href="#" class="js-gallery-grid-item" data-title="<?php echo $product['post_title']; ?>" data-id="<?php echo $product['product_id']; ?>" data-image="<?php echo $image; ?>">
                                                <img src="<?php echo $image;?>" alt="<?php echo $product['post_title']; ?>">
                                            </a>
                                        </div>
                                        <div class="gallery__grid-item-content">
                                            <h5 class="gallery__grid-item-title">
                                                <a href="#" class="js-gallery-grid-item" data-title="<?php echo $product['post_title']; ?>" data-id="<?php echo $product['product_id']; ?>" data-image="<?php echo $image; ?>">
                                                    <?php echo $product['post_title']; ?><br>
                                                    <span><?php echo $term_name; ?></span>
                                                </a>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>



    <!-- TEST -->
    <!--	<a href="#" data-ids="2773,2775, 2777, 148" class="addToCartBtn" >addToCartBtn</a> -->
    <script>
      jQuery(document).ready(function($){

        $('.addToCartBtn').click(function (e) {
          e.preventDefault();

          var dataHrefs = $(this).attr('data-ids');
          var datHrefsArray = dataHrefs.split(",");
          var filteredHrefs = datHrefsArray.filter(function (el) {
            return el != "";
          });
          console.log(filteredHrefs); /* Successfully outputs the product IDs in an array */

          filteredHrefs.forEach(function(item){
            console.log(item); /* Successfully outputs each product ID as a string on a separate line */

            var data = {
              action: 'woocommerce_ajax_add_to_cart',
              product_id: item,
            };
            $.ajax({
              type: 'post',
              url: wc_add_to_cart_params.ajax_url,
              data: data,
              success: function (response) {
                console.log('success');
              },
            });

            return false;
          });
        });
      });
    </script>
  <?php
}

add_shortcode( 'calacatta_superior', 'calacatta_superior' );
?>
