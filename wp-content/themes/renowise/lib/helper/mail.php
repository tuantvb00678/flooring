<?php
/**
 * Set up email variables
 * @param $user
 * @param $subject
 * @param $body
 * @return mixed
 */
function setup_email_dynamic_variables ($user, $subject, $body) {
  $placeholders = [
    "{{ CONTACT_NAME }}",
    "{{ CONTACT_MAIL }}",
    "{{ CONTACT_PHONE }}",
    "{{ CONTACT_ADDRESS }}",
    "{{ CONTACT_PROJECT_BEGIN }}",
    "{{ CONTACT_FOUND_YOU_VIA }}",
    "{{ PIECES_DATA }}",
    "{{ PIECES_OPTION }}",
    "{{ STONE_COLOR }}",
    "{{ EDGE_DATA }}",
    "{{ CUT_OUTS_DATA }}",
    "{{ EXTRAS_DATA }}",
    "{{ TOTAL_PRICE }}",
  ];
  if ( !empty($body) || !empty($subject) ) {
    $data = [
      $user['contact_name'],
      $user['contact_email'],
      $user['contact_phone'],
      $user['contact_address'],
      $user['contact_project_begin'],
      $user['contact_found_you_via'],
      $user['pieces_data'],
      $user['pieces_option'],
      $user['stone_color'],
      $user['edge_data'],
      $user['cut_outs_data'],
      $user['extras_data'],
      $user['total_price'],
    ];
    return str_replace($placeholders, $data, $body);
  }
}


/**
 * Send mail
 * @param $user
 * @param $subject_field
 * @param $body_field
 */
function quote_send_mail($user, $subject_field, $body_field) {
    $subject = $subject_field;
    $body = $body_field;
    $separator = md5(time());
    $eol = PHP_EOL;
    $headers = "MIME-Version: 1.0".$eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol;
    $headers .= "Content-Transfer-Encoding: 7bit".$eol;

    if ( !empty($body) || !empty($subject) ) {
        $to = $user['email'];
        $message = setup_email_dynamic_variables($user, $subject, $body);

        $attachments = array();
        $attachment_id = ''; // generate_pdf($user);
        if (!empty($attachment_id)) {
            $attachment_url = wp_get_attachment_url($attachment_id);
            $pdf_url = str_replace(home_url('wp-content'), WP_CONTENT_DIR, $attachment_url);
            $attachments = array($pdf_url);
            // write_log(sprintf('PDF "%s" is generated for "%s"', get_the_title($attachment_id), $to));
        } else {
            // write_log('PDF failed to generated for %s"', $to);
        }

        $mail_sent = wp_mail($to, $subject, $message, $headers, $attachments);
        if ($mail_sent) {
            // write_log(sprintf('Email was sent to "%s"', $to));
            // send mail success update certification_sent_indicator = complete
        } else {
//            write_log(sprintf('Email failed to sent to "%s" - Error: "%s"', $to, $mail_sent));
        }
//        if (!empty($attachment_id)) {
//            // delete attachment pdf file when send success
//            wp_delete_attachment($attachment_id);
//        }
    }
}

/**
 * Write log files
 * @param $message
 */
function write_log($message) {
    if(is_array($message)) {
        $message = json_encode($message);
    }
    $current_time = current_time('timestamp');
    $current_time_in_nice_format = date('d/m/Y H:i', $current_time);

    /**
     * get current log
     */
    $logs = get_option('custom-logs');

    if ($logs == false) {
        $logs = array();
    }

    /**
     * if the array length is greater than 200 then lets remove the first item
     */
    if(count($logs) > 200){
        array_shift($logs);
    }

    /**
     * now lets add our new item to the log
     */
    array_push($logs, array($current_time_in_nice_format, $message));

    /**
     * update the setting
     */
    update_option('custom-logs', $logs);
}


?>
