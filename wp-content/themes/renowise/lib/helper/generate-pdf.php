<?php

use setasign\Fpdi\Fpdi;

class GeneratePDF {
  /**
   * GeneratePDF constructor.
   */
  public function __construct() {
    $this->pdf = new Fpdi();

    $this->pdf_file = get_template_directory() . '/custom/pdf/quote_pdf.pdf';
    $this->setup();
  }

  /**
   * Setup
   */
  public function setup() {
    $this->pdf->setSourceFile($this->pdf_file);
    $first_page = $this->pdf->importPage(1);
    $specs = $this->pdf->getTemplateSize($first_page);
    $this->pdf->AddPage($specs['height'] > $specs['width'] ? 'P' : 'L');
    $this->pdf->useTemplate($first_page);
    $this->set_style();
  }

  /**
   * Set Style
   */
  public function set_style() {
    $this->pdf->SetFont('Helvetica');
  }

  /**
   * Set Price
   * @param string $text
   */
  public function set_price($text) {
    $this->pdf->SetFont('Times', 'B', 15);
    $this->pdf->SetTextColor(0, 0, 0);
    $this->pdf->SetXY(118, 201);
    $this->pdf->MultiCell(0, 0, $text, 0, 'L', false);
  }

  /**
   * Set Name
   * @param string $text
   */
  public function set_name($text) {
    $this->pdf->SetFont('Times', '', 11);
    $this->pdf->SetTextColor(0, 0, 0);
    $this->pdf->SetXY(25, 78.5);
    $this->pdf->MultiCell(0, 0, $text, 0, 'L', false);
  }

  /**
   * Set Email
   * @param string $text
   */
  public function set_email($text) {
    $this->pdf->SetFont('Times', '', 11);
    $this->pdf->SetTextColor(0, 0, 0);
    $this->pdf->SetXY(25, 83.5);
    $this->pdf->MultiCell(0, 0, $text, 0, 'L', false);
  }

  /**
   * Set Phone
   * @param string $text
   */
  public function set_phone($text) {
    $this->pdf->SetFont('Times', '', 11);
    $this->pdf->SetTextColor(0, 0, 0);
    $this->pdf->SetXY(25, 88.5);
    $this->pdf->MultiCell(0, 0, $text, 0, 'L', false);
  }

  /**
   * Set brand color
   * @param string $text
   */
  public function set_brand_color($text) {
    $this->pdf->SetFont('Times', '', 11);
    $this->pdf->SetTextColor(0, 0, 0);
    $this->pdf->SetXY(10, 112.5);
    $this->pdf->MultiCell(0, 0, $text, 0, 'L', false);
  }

  public function set_edge_work($text) {
    $this->pdf->SetFont('Times', '', 11);
    $this->pdf->SetTextColor(0, 0, 0);
    $this->pdf->SetXY(10, 128.5);
    $this->pdf->MultiCell(0, 0, $text, 0, 'L', false);
  }

  public function set_cut_outs($text, $y = 142) {
    $this->pdf->SetFont('Times', '', 11);
    $this->pdf->SetTextColor(0, 0, 0);
    $this->pdf->SetXY(10, $y);
    $this->pdf->MultiCell(0, 0, $text, 0, 'L', false);
  }

  public function set_extras($text, $y = 162) {
    $this->pdf->SetFont('Times', '', 11);
    $this->pdf->SetTextColor(0, 0, 0);
    $this->pdf->SetXY(10, $y);
    $this->pdf->MultiCell(0, 0, $text, 0, 'L', false);
  }

  /**
   * Set title
   * @param string $title
   */
  public function set_title($title) {
    $this->pdf->SetFont('Times', 'B', 20);
    $this->pdf->SetTextColor(118, 145, 59);
    $title_width = $this->pdf->GetStringWidth($title);
    $title_max_width = 280;
    $title_xy = $title_width > $title_max_width ? 135 : 138;
    $this->pdf->SetXY(0, $title_xy);
    $this->pdf->MultiCell($title_max_width, 6, $title, 0, 'C', false);
  }

  /**
   * Set Date
   * @param string $date
   */
  public function set_date($date) {
    $this->pdf->SetFont('Times', 'B', 26);
    $this->pdf->SetTextColor(0, 113, 187);
    $this->pdf->SetXY(1, 155);
    $this->pdf->MultiCell(120, 0, $date, 0, 'C', false);
  }

  /**
   * Write content
   * @param $text
   * @param string $type
   * @param $y
   */
  public function write($text, $type, $y = 0) {
    if ( empty($text) ) {
      return;
    }

    switch($type) {
      case 'price':
        $this->set_price($text);
        break;
      case 'date':
        $this->set_date($text);
        break;
      case 'name':
        $this->set_name($text);
        break;
      case 'email':
        $this->set_email($text);
        break;
      case 'phone':
        $this->set_phone($text);
        break;
      case 'brand_color':
        $this->set_brand_color($text);
        break;
      case 'edge_work':
        $this->set_edge_work($text);
        break;
      case 'cut_outs':
        $this->set_cut_outs($text, $y);
        break;

      case 'extras':
        $this->set_extras($text, $y);
        break;
    }
  }

  /**
   * Export pdf file
   * @param int $post_id
   * @param string $pdf_name
   * @return int|WP_Error
   */
  public function export($pdf_name, $post_id) {
    $wp_uploads = wp_upload_dir();
    $file_name = $pdf_name . '.pdf';
    $this->pdf->output('F', $wp_uploads['path'] . '/' .$file_name, true);
    $file_path = $wp_uploads['url'] . '/' .$file_name;

    return $file_path; //$this->save_attachment($file_path, $file_name, $post_id);
  }

  /**
   * Save attachment file
   * @param $file_path
   * @param $file_name
   * @param int $parent_post_id
   * @return int|WP_Error
   */
  public function save_attachment ($file_path, $file_name,  $parent_post_id) {
    // Filename should be the path to a file in the upload directory.
    if (empty($file_path)) return 0;

    // The ID of the post this attachment is for.
    if (empty($parent_post_id)) return 0;

    // Prepare an array of post data for the attachment.
    $attachment = array(
      'guid'           => $file_path,
      'post_title'     => $file_name,
      'post_status'    => 'inherit'
    );

    // Insert the attachment.
    return wp_insert_attachment( $attachment, $file_path, $parent_post_id );
  }
}
