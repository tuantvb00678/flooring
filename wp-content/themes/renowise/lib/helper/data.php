<?php

function get_products_by_category_slug($slug ='', $is_search = false, $str = '', $taxonomy = 'product_cat') {
  global $wpdb;
  $sql_slug = '';
  $sql_search = '';

  if ( !empty($slug) && is_string($slug)) {
    $sql_slug = " AND t.slug IN ('{$slug}')";
  }

  if ( !empty($slug) && is_array($slug)) {
    $term_ids = implode(', ', $slug);
    $sql_slug = " AND t.term_id IN ({$term_ids})";
  }

  if ( $is_search && !empty($str) ) {
    $sql_search = " AND p.post_title '%{$str}%'";
  }


  $sql = "SELECT 
                p.ID as product_id,
                p.post_title,
                `post_content`,
                `post_excerpt`,
                t.name as term_name,
                t.term_id AS term_id,
                t.slug AS term_slug,
                tt.term_taxonomy_id AS tt_term_taxonomia,
                tr.term_taxonomy_id AS tr_term_taxonomia,
                MAX(CASE WHEN pm1.meta_key = '_price' then pm1.meta_value ELSE NULL END) as price,
                MAX(CASE WHEN pm1.meta_key = '_regular_price' then pm1.meta_value ELSE NULL END) as regular_price,
                MAX(CASE WHEN pm1.meta_key = '_sale_price' then pm1.meta_value ELSE NULL END) as sale_price,
                MAX(CASE WHEN pm1.meta_key = '_sku' then pm1.meta_value ELSE NULL END) as sku 
              FROM wp_posts p 
              LEFT JOIN wp_postmeta pm1 ON pm1.post_id = p.ID
              LEFT JOIN wp_term_relationships AS tr ON tr.object_id = p.ID
              JOIN wp_term_taxonomy AS tt ON tt.taxonomy = '{$taxonomy}' AND tt.term_taxonomy_id = tr.term_taxonomy_id 
              JOIN wp_terms AS t ON t.term_id = tt.term_id
              WHERE p.post_type in('product', 'product_variation') AND p.post_status = 'publish' {$sql_slug} {$sql_search}
              GROUP BY p.ID,p.post_title ORDER BY p.post_title ASC";


  $products = $wpdb->get_results(
    $sql,
    ARRAY_A
  );

  return $products;
}

function get_product_by_acf($step1) {
  $product_id = 0;
  if ( !empty($step1) ) {
    $first = $step1[0];
    $product_id = $first->ID;
  }
  return $product_id;
}

function get_product_by_id($product_id, $taxonomy = 'product_cat') {
  global $wpdb;
  $row = array();

  $sql_search = " AND p.ID = {$product_id}";
  $products = $wpdb->get_results(
    "SELECT 
                p.ID as product_id,
                p.post_title,
                `post_content`,
                `post_excerpt`,
                t.term_id AS term_id,
                t.slug AS term_slug,
                tt.term_taxonomy_id AS tt_term_taxonomia,
                tr.term_taxonomy_id AS tr_term_taxonomia,
                MAX(CASE WHEN pm1.meta_key = '_price' then pm1.meta_value ELSE NULL END) as price,
                MAX(CASE WHEN pm1.meta_key = '_regular_price' then pm1.meta_value ELSE NULL END) as regular_price,
                MAX(CASE WHEN pm1.meta_key = '_sale_price' then pm1.meta_value ELSE NULL END) as sale_price,
                MAX(CASE WHEN pm1.meta_key = '_sku' then pm1.meta_value ELSE NULL END) as sku 
              FROM wp_posts p 
              LEFT JOIN wp_postmeta pm1 ON pm1.post_id = p.ID
              LEFT JOIN wp_term_relationships AS tr ON tr.object_id = p.ID
              JOIN wp_term_taxonomy AS tt ON tt.taxonomy = '{$taxonomy}' AND tt.term_taxonomy_id = tr.term_taxonomy_id 
              JOIN wp_terms AS t ON t.term_id = tt.term_id
              WHERE p.post_type in('product', 'product_variation') AND p.post_status = 'publish' {$sql_search}
              GROUP BY p.ID,p.post_title",
    ARRAY_A
  );

  if ( !empty($products) ) {
    $first = $products[0];
    $row = $first;
  }

  return $row;
}

function get_categories_by_product_id($product_id, $term_id, $is_slug = true) {
  global $wpdb;
  $term_slugs = array();

  if ( !empty($term_id) && is_array($term_id)) {
    $term_ids = implode(', ', $term_id);
    $sql_term = " AND t.term_id IN ({$term_ids})";
  } else {
    $sql_term = " AND t.term_id IN ({$term_id})";
  }

  if ($is_slug) {
    $sql_col = ' t.slug AS term_slug ';
  } else {
    $sql_col = ' t.name AS term_name ';
  }

  $sql = "SELECT
                {$sql_col}
              FROM wp_posts p 
              LEFT JOIN wp_postmeta pm1 ON pm1.post_id = p.ID
              LEFT JOIN wp_term_relationships AS tr ON tr.object_id = p.ID
              JOIN wp_term_taxonomy AS tt ON tt.taxonomy = 'product_cat' AND tt.term_taxonomy_id = tr.term_taxonomy_id 
              JOIN wp_terms AS t ON t.term_id = tt.term_id
              WHERE p.post_type in('product', 'product_variation') AND p.post_status = 'publish'  
              {$sql_term} 
              AND p.id = {$product_id}
              GROUP BY p.ID, t.term_id ORDER BY p.post_title ASC";
  $cols = $wpdb->get_col($sql);
  if ( !empty($cols) ) {
    $term_slugs = $cols;
  }

  return $term_slugs;
}

function query_product_cat_child($parent_id) {
  global $wpdb;

  $term_taxonomy = $wpdb->prefix.'term_taxonomy';
  $terms = $wpdb->prefix.'terms';

  $sql = "SELECT {$terms}.term_id FROM {$term_taxonomy} JOIN {$terms} ON {$term_taxonomy}.term_id = {$terms}.term_id
        WHERE {$term_taxonomy}.parent = {$parent_id} AND taxonomy = 'product_cat'";
//  echo "<pre>"; print_r($sql); die;
  $rows= $wpdb->get_col($sql);

  return $rows;
}

//$term_ids = query_product_cat_child(54);
//$brands = get_product_cat_child($term_ids);
//ECHO "<PRE>"; print_r($brands); die;

function get_product_cat_child($term_ids = array()) {
  $product_cats = array();

  if (!empty($term_ids)) {
    $args = array(
      'taxonomy'               => 'product_cat',
      'orderby'                => 'name',
      'order'                  => 'ASC',
      'hide_empty'             => false,
      'term_taxonomy_id'       => $term_ids
    );
    $terms = new WP_Term_Query($args);

    foreach ($terms->get_terms() as $term) {
      $option = get_field('type', $term);
      $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
      $image = wp_get_attachment_url( $thumbnail_id );
      $product_cats[] = array(
        'term_id' => $term->term_id,
        'name' => $term->name,
        'des' => $term->description,
        'slug' => $term->slug,
        'option' => $option,
        'image' => (!empty($image)) ? $image : get_default_image()
      );
    }
  }
  return $product_cats;
}

// get product tag
function get_product_tags($option_type) {
  $tag_args = array(
    'taxonomy'               => 'product_tag',
    'orderby'                => 'name',
    'order'                  => 'ASC',
    'hide_empty'             => false,
  );
  $tag_query = new WP_Term_Query($tag_args);
  $product_tags = array();
  if (!empty($tag_query)) {
    foreach($tag_query->get_terms() as $term){
      $image = get_field('image_tag', $term);
      $url = $image['url'];
      $option = get_field('option_type', $term);
      if ($option == $option_type) {
        $product_tags[] = array(
          'term_id' => $term->term_id,
          'name' => $term->name,
          'slug' => $term->slug,
          'option' => $option,
          'image' => (!empty($image)) ? $url : get_default_image()
        );
      }
    }
  }
  return $product_tags;
}

function get_id_brand_by_slug($slug) {
  global $wpdb;

  $terms = $wpdb->prefix.'terms';

  $rows= $wpdb->get_row(
    "SELECT * FROM {$terms}
        WHERE slug = '{$slug}'");

  return $rows;

}

function get_brand($parent_id = 0) {
  $term_ids = query_product_cat_child($parent_id);
  $product_tags = get_product_cat_child($term_ids);

  return $product_tags;
}

function get_default_image() {
  return get_template_directory_uri(). '/custom/img/default.jpg';
}

?>
