<?php if ( !empty($products) ) : ?>
<div class="search-products">
    <?php
    $first_product_image = get_the_post_thumbnail_url($products[0]['product_id']);
    if (empty($first_product_image)) {
      $first_product_image = get_default_image();
    }
    ?>
  <div class="search-item search-item--first js-search-item-first" data-id="<?php echo $products[0]['product_id']; ?>" data-title="<?php echo $products[0]['post_title']; ?>" data-price="<?php echo $products[0]['price']; ?>" data-image="<?php echo $first_product_image; ?>">
    <div class="search-item__inner">
      <div class="search-image">
        <img src="<?php echo $first_product_image; ?>" alt="<?php echo $products[0]['post_title']; ?>" width="326" height="270">
      </div>
      <h6><?php echo $products[0]['post_title']; ?></h6>
    </div>
  </div>

  <div class="search-item--second">
    <div class="js-search-grid search-grid search-grid--ajax">
      <?php foreach ($products as $key => $product) :
        $product_image = get_the_post_thumbnail_url($product['product_id']);
        if (empty($product_image)) {
          $product_image = get_default_image();
        }
        $selected = ($key == 0) ? ' search-item--selected' : ''; ?>
        <div class="js-search-item search-item<?php echo $selected; ?>" data-id="<?php echo $product['product_id']; ?>" data-price="<?php echo $product['price']; ?>">
          <div class="search-item__inner">
            <div class="search-image">
              <img src="<?php echo $product_image; ?>" alt="<?php echo $product['post_title']; ?>">
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
<?php endif; ?>
