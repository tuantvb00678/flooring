<?php if ( !empty($brands) ) : ?>
<div class="ajax-brands">
  <h5><?php echo $group_name . ' ' . __('Group', 'renowise'); ?></h5>
  <div class="search-grid search-grid--sub-brands js-ajax-sub-brand">
    <?php foreach ($brands as $brand) : ?>
      <div class="search-item search-item--brand js-ajax-sub-brand-item" data-id="<?php echo $brand['term_id']; ?>" data-name="<?php echo $brand['name']; ?>" data-slug="<?php echo $brand['slug']; ?>">
        <div class="search-item__inner">
          <div class="search-image">
            <img src="<?php echo $brand['image']?>" alt="<?php echo $brand['name']?>">
            <h6><?php echo $brand['name']; ?></h6>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
<?php endif; ?>
