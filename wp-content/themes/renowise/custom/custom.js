jQuery(document).ready(function($){

	setTimeout(function(){
		$('#overlay').fadeOut(300);
	},11500);

	var el = $('.quote-caculator');
	// base function
	var bt_parse_float = function( x ) {
		r = parseFloat( x );
		if ( isNaN( r ) ) r = 0;
		return r;
	}
	var bt_parse_int = function( x ) {
		r = parseInt( x );
		if ( isNaN( r ) ) r = 0;
		return r;
	}

	// Push array data
	var push_data = function () {
		var pieceData = {
			'product_id' : $('.js-step1-pieces').data('id'),
			'title' : $('.js-step1-pieces').data('title'),
			'price' : $('.js-step1-pieces').data('price'),
		};

		var pieceOptionData = [];
		var table = el.find('.js-step1-table');
		table.find('.js-step1-row').each(function(index) {
			var width = parseInt($(this).find('.js-step1-width').val());
			var length = parseInt($(this).find('.js-step1-length').val());

			var item = {
				'index': index + 1,
				'width' : width,
				'length' : length,
			};
			pieceOptionData.push(item);
		});

		// color data
		var searchItemFirst = $('.js-search-item-first');
		var colorData = {};
		if (searchItemFirst) {
			colorData = {
				'product_id': searchItemFirst.attr('data-id'),
				'title': searchItemFirst.attr('data-title'),
				'price': bt_parse_float(searchItemFirst.attr('data-price')),
				'qty': 1,
				'image': searchItemFirst.attr('data-image'),
			};
		}

		// Edge Profiles
		var edgeGrid = $('.js-edge-grid');
		if (edgeGrid) {
			var edgeData = {
				'product_id' : edgeGrid.attr('data-id'),
				'title' : edgeGrid.attr('data-title'),
				'price' : bt_parse_float(edgeGrid.attr('data-price')),
				'qty' : 1,
				'image' : edgeGrid.attr('data-image')
			};
		}

		// cut-outs
		var cutOutsData = [];
		var cutoutsInput = $('.js-cutouts-inputs');
		var cutoutInput = $('.js-cutout-input');
		if (cutoutsInput && cutoutInput.length > 0) {
			$('.js-cutout-input').each(function () {
				var qty = bt_parse_int($(this).val());
				if (qty > 0) {
					var element = {
						'product_id': $(this).data('id'),
						'title': $(this).data('title'),
						'price': $(this).data('price'),
						'qty': qty
					};
					cutOutsData.push(element);
				}
			});
		}

		// Extras Data
		var extrasData = [];
		var extrasInput = $('.js-extra-inputs');
		var extraInput = $('.js-extra-input');
		if (extrasInput && extraInput.length > 0) {
			$('.js-extra-input').each(function () {
				var qty = bt_parse_int($(this).val());
				if (qty > 0) {
					var element = {
						'product_id': $(this).data('id'),
						'title': $(this).data('title'),
						'price': $(this).data('price'),
						'qty': qty
					};
					extrasData.push(element);
				}
			});
		}

		var data = {
			'pieceData' : pieceData,
			'pieceOptionData' : pieceOptionData,
			'colorData' : colorData,
			'edgeData' : edgeData,
			'cutOutsData' : cutOutsData,
			'extrasData' : extrasData
		};

		console.log(data);

		return data;
	}

	// binding data to step final
	var binding_final_step = function(data) {
		var pieceData = data['pieceData'];
		var pieceOptionData = data['pieceOptionData'];

		var pieceOption = $('.js-final-piece-option');
		pieceOption.html('');

		if (pieceOptionData.length > 0) {
			var pieceOptionHtml = '';
			$.each(pieceOptionData, function (index, value) {
				pieceOptionHtml += '<h6>' + value.index + '. Piece: width: ' + value.width + 'mm - Length: ' + value.length + 'mm</h6>';
			});
			pieceOption.html(pieceOptionHtml);
		} else {
			$('.js-final-piece-not-found').removeClass('disabled');
		}

		var colorData = data['colorData'];
		if (typeof colorData['image'] == 'undefined') {
			$('.js-final-colour-item').addClass('disabled')
		} else {
			$('.js-final-colour-item').removeClass('disabled')
			$('.js-final-colour-title').text(colorData['title']);
			$('.js-final-colour-img').attr('src', colorData['image']);
			$('.js-step8-content-select').html(colorData['title']);
		}

		var edgeData = data['edgeData'];
		if (edgeData['image'] == '') {
			$('.js-final-edge-thickness').addClass('disabled');
		} else {
			$('.js-final-edge-thickness').removeClass('disabled');
			$('.js-final-edge-title').text(edgeData['title']);
			$('.js-final-edge-thumb').attr('src', edgeData['image']);
		}

		$('.js-final-cutouts').html('');
		var cutOutsData = data['cutOutsData'];
		if (cutOutsData.length > 0) {
			var cutOutsHtml = '';
			$.each(cutOutsData, function (index, value) {
				cutOutsHtml += '<h6>' + value.title + ' - ' + value.qty + ' Qty</h6>';
			});
			$('.js-final-cutouts').html(cutOutsHtml);
		} else {
			$('.js-final-cutouts-not-found').removeClass('disabled');
		}

		$('.js-final-extras').html('');
		var extrasData = data['extrasData'];
		if (extrasData.length > 0) {
			var extrasHtml = '';
			$.each(extrasData, function (index, value) {
				extrasHtml += '<h6>' + value.title + ' - ' + value.qty + ' Qty</h6>';
			});
			$('.js-final-extras').html(cutOutsHtml);
		} else {
			$('.js-final-extras-not-found').removeClass('disabled');
		}

	}

	var data = push_data();
	binding_final_step(data);

	// Calculator
	var quote_total = function( c ) {
		var c = $( c );
		total = 0;

		console.log('--- Quote Calculator ---');
		total += quote_input(c, '.js-step1-pieces');
		console.log('+ Step1 - pieces Price: ' + total);

		var searchItemFirst = $('.js-search-item-first');
		if (searchItemFirst) {
			var searchPrice = bt_parse_float(searchItemFirst.attr('data-price'));
			total += searchPrice;
			console.log('+ Step2 - Search Price: ' + searchPrice);
		}

		var edgeGrid = $('.js-edge-grid');
		if (edgeGrid) {
			var edgeGridPrice = bt_parse_float(edgeGrid.attr('data-price'));
			total += edgeGridPrice;
			console.log('+ Step3 - Edges Price: ' + edgeGridPrice);
		}

		// cut-outs
		var cutoutsInput = $('.js-cutouts-inputs');
		var cutoutInput = $('.js-cutout-input');
		if (cutoutsInput && cutoutInput.length > 0) {
			var cutoutsPrice = 0;
			$('.js-cutout-input').each(function () {
				total += total_cutout_input($(this));
				cutoutsPrice += total_cutout_input($(this));
			});
			console.log('+ Step4 - Cut-Outs Price: ' + cutoutsPrice);
		}

		// extras
		var extrasInput = $('.js-extra-inputs');
		var extraInput = $('.js-extra-input');
		if (extrasInput && extraInput.length > 0) {
			var extrasPrice = 0;
			$('.js-extra-input').each(function () {
				total += total_extra_input($(this));
				extrasPrice += total_extra_input($(this));
			});
			console.log('+ Step5 - Extras Price: ' + extrasPrice);
		}

		total = total.toFixed(2);
		console.log('+ Step6 - Total Price: ' + extrasPrice);
		c.find( '.js-quote-total-price' ).html( total );
		c.find( '.js-step8-price-num' ).html( total );
		c.find( '.js-quote-submit-button' ).attr( 'data-total', total );
	}


	// total price when input change
	var quote_input = function (c, childInput) {
		var total = 0;
		var input = c.find( childInput );
		var unit_price = bt_parse_float( input.data( 'price' ) );
		var qtyPieces = bt_parse_float( input.val() ); // qty pieces

		if (qtyPieces > 0) {
			var mm = 0;
			var table = c.find('.js-step1-table');
			table.find('.js-step1-row').each(function(index) {
				var width = parseInt($(this).find('.js-step1-width').val());
				var length = parseInt($(this).find('.js-step1-length').val());
				console.log('width row - ' + index + ': ' + width);
				console.log('length row - ' + index + ': ' + length);
				mm += length * width * unit_price
			});
		}

		total += mm;

		return total;
	}

	// validation nunber in input
	var quote_validation_input = function(element) {
		var position = element.selectionStart - 1;
		//remove all but number and .
		var fixed = element.value.replace(/[^0-9]/g, "");

		if (element.value !== fixed) {
			element.value = fixed;
			element.selectionStart = position;
			element.selectionEnd = position;
		}
	}
	// event input
	var quote_keyup_input = function (childClass) {
		$('body').on('keyup', childClass, function() {
			quote_validation_input(this);
			var value = $(this).val();

			quote_total( el );
			binding_final_step(data);
		});
	}
	var quote_keyup_pieces = function() {
		var table = $('.js-step1-table');
		var input = $('.js-step1-pieces');
		input.bind('keyup', function() {
			quote_validation_input(this);
			var value = $(this).val();
			var p = parseInt( value );
			var num = 1;
			var i = 0;
			table.find('.js-step1-row').not(':first').remove();
			for(i=num; i<p; i+=1){
				table.find('.js-step1-row').first().clone().appendTo('.js-step1-table');
			}

			var step1Length = $('.step1').innerHeight();
			var heightProgressBar = progressBarEl.innerHeight();
			el.height(step1Length + heightProgressBar + 100);

			// calculate
			quote_total( el );
			var data = push_data();
			binding_final_step(data);
		});
	}

	var quote_caculator_init = function() {
		quote_keyup_pieces();
		quote_keyup_input( '.js-step1-width' );
		quote_keyup_input( '.js-step1-length' );
	}

	quote_caculator_init(el);

	//---------- search selected color

	$('body').on('click', '.js-search-item', function () {
		var searchItemFirst = $('.js-search-item-first');
		var searchItemImageFirst = searchItemFirst.find('img');

		$('.js-search-grid .js-search-item').removeClass('search-item--selected');
		$(this).addClass('search-item--selected');

		var itemTitle = $(this).find('img').attr('alt');
		var imageSrc = $(this).find('img').attr('src');
		var itemId = $(this).data('id');
		var itemPrice = $(this).data('price');

		// change image first and binding data to first item
		searchItemFirst.find('h6').text(itemTitle);
		searchItemFirst.attr('data-id', itemId);
		searchItemFirst.attr('data-title', itemTitle);
		searchItemFirst.attr('data-price', itemPrice)
		searchItemFirst.attr('data-image', imageSrc);
		searchItemImageFirst.attr('src', imageSrc);

		quote_total( el );
		var data = push_data();
		binding_final_step(data);
	});


	//---------- selected edge
	var edgeGrid = $('.js-edge-grid');
	if (edgeGrid) {
		$('.js-edge-grid .edge-item').click(function () {
			$('.js-edge-grid .edge-item').removeClass('edge-item--selected');
			$(this).addClass('edge-item--selected');
			var itemId = $(this).data('id');
			var itemTitle = $(this).data('title');
			var itemPrice = $(this).data('price');
			var itemImage = $(this).data('image');

			edgeGrid.attr('data-id', itemId)
			edgeGrid.attr('data-title', itemTitle)
			edgeGrid.attr('data-price', itemPrice)
			edgeGrid.attr('data-image', itemImage)

			quote_total(el);
			var data = push_data();
			binding_final_step(data);
		});
	}

	//---------- cutouts input
	var cutoutsInput = $('.js-cutouts-inputs');
	var cutoutInput = $('.js-cutout-input');
	if (cutoutsInput && cutoutInput.length > 0) {
		$('.js-cutout-input').each(function () {
			$(this).keyup(function () {
				quote_validation_input(this);
				quote_total(el);
				var data = push_data();
				binding_final_step(data);
			});
		});

		var total_cutout_input = function (inputEl) {
			var total = 0;
			var unit_price = bt_parse_float(inputEl.data('price'));
			var val = bt_parse_float(inputEl.val());
			val = val * unit_price;
			total += val;
			return total;
		}
	}

	//---------- extras input
	var extrasInput = $('.js-extra-inputs');
	var extraInput = $('.js-extra-input');
	if (extrasInput && extraInput.length > 0) {
		$('.js-extra-input').each(function () {
			$(this).keyup(function () {
				quote_validation_input(this);
				quote_total(el);
				var data = push_data();
				binding_final_step(data);
			});
		});

		var total_extra_input = function (inputEl) {
			var total = 0;
			var unit_price = bt_parse_float(inputEl.data('price'));
			var val = bt_parse_float(inputEl.val());
			val = val * unit_price;
			total += val;
			return total;
		}
	}


	//----------jQuery time slider step form
	var current_fs, next_fs, previous_fs; //fieldsets
	var left, opacity, scale; //fieldset properties which we will animate
	var animating; //flag to prevent quick multi-click glitches
	var progressBarEl = $('#progressbar');


	$('.js-start-quote').click(function() {
		$('#progressbar li:first').addClass('active');
		var step0 = $('.step0');
		var next_step1 = $('.step1');

		// height
		var splashArray = new Array();
		el.find('.step').each(function() {
			splashArray.push($(this).innerHeight());
		});
		var heightProgressBar = progressBarEl.innerHeight();
		// end height

		var heightStep = parseInt(splashArray[0]);
		el.height(heightStep + heightProgressBar + 100);

		step0.animate({opacity: 0}, {
			step: function(now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in 'now'
				//1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.2;
				//2. bring next_fs from the right(50%)
				left = (now * 50)+'%';
				//3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now;
				step0.css({
					'transform': 'scale('+scale+')',
					'position': 'absolute'
				});
				next_step1.css({
					'left': left,
					'opacity': opacity,
					'position': 'absolute',
					'display': 'block'
				});
			},
			duration: 800,
			complete: function(){
				step0.hide();
				animating = false;
			},
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});
	});

	$('.next').click(function() {

		if($(this).hasClass('active')) {
			quote_total( el );
			var data = push_data();
			binding_final_step(data);
		}

		if(animating) return false;
		animating = true;

		current_fs = $(this).parent();
		next_fs = $(this).parent().next();

		// height
		var splashArray = new Array();
		el.find('.step').each(function() {
			splashArray.push($(this).innerHeight());
		});
		var heightProgressBar = progressBarEl.innerHeight();
		// end height

		var index = current_fs.attr('data-index');
		var numberIndex = parseInt(index);
		var heightStep = parseInt(splashArray[numberIndex]);
		el.height(heightStep + heightProgressBar + 200);

		//activate next step on progressbar using the index of next_fs
		$('#progressbar li').eq($('.step').index(next_fs)).addClass('active');

		//show the next fieldset
		next_fs.show();
		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
			step: function (now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in 'now'
				//1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.2;
				//2. bring next_fs from the right(50%)
				left = (now * 50) + '%';
				//3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({
					'transform': 'scale(' + scale + ')',
					'position': 'absolute'
				});
				next_fs.css({'left': left, 'opacity': opacity});
			},
			duration: 800,
			complete: function () {
				current_fs.hide();
				animating = false;
			},
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});

		$('.js-ajax-content').addClass('disabled');
		$('.js-step2-prev').removeClass('ajax-active');
		$('.js-step2-content-form').removeClass('disabled');

		$('.step7-msg-success').addClass('disabled');
		$('.js-quote-submit-button').removeClass('disabled');
		$('.js-step7-next-button').addClass('disabled');

	});

	$('.previous').click(function(e) {
		e.preventDefault();
		if ($('.js-ajax-content').hasClass('disabled')) {
			$(this).removeClass('ajax-active');

			if(animating) return false;
			animating = true;

			current_fs = $(this).parent();
			previous_fs = $(this).parent().prev();

			// height
			var splashArray = new Array();
			el.find('.step').each(function() {
				splashArray.push($(this).innerHeight());
			});
			var heightProgressBar = progressBarEl.innerHeight();
			// end height

			var index = current_fs.attr('data-index');
			var numberIndex = parseInt(index);
			var heightStep = parseInt(splashArray[numberIndex-2]);
			el.height(heightStep + heightProgressBar + 100);

			console.log(numberIndex - 2);
			console.log(heightProgressBar);
			console.log(heightStep);
			console.log(splashArray);

			//de-activate current step on progressbar
			$('#progressbar li').eq($('.step').index(current_fs)).removeClass('active');

			//show the previous fieldset
			previous_fs.show();
			//hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
				step: function(now, mx) {
					//as the opacity of current_fs reduces to 0 - stored in 'now'
					//1. scale previous_fs from 80% to 100%
					scale = 0.8 + (1 - now) * 0.2;
					//2. take current_fs to the right(50%) - from 0%
					left = ((1-now) * 50)+'%';
					//3. increase opacity of previous_fs to 1 as it moves in
					opacity = 1 - now;
					current_fs.css({'left': left});
					previous_fs.css({
						'transform': 'scale('+scale+')',
						'opacity': opacity,
					});
				},
				duration: 800,
				complete: function(){
					current_fs.hide();
					animating = false;
				},
				//this comes from the custom easing plugin
				easing: 'easeInOutBack'
			});

		} else {
			if ($(this).hasClass('ajax-active')) {
				$('.js-ajax-content').addClass('disabled');
				$('.js-step2-content-form').removeClass('disabled');
			}
		}

	});

	var selectedList = function(id) {
		$('.js-step8-selected').each(function (index, value) {
			var dataId = $(this).attr('data-id');
			console.log('-------selected list: ');
			console.log(dataId)
			var text = $(this).find('.js-step8-text');
			var label = $(this).find('.js-step8-label');
			if (dataId == id) {
				text.addClass('disabled');
				label.removeClass('disabled');
			}
		});
	}

	// click brand item search ajax
	var ajaxBrand = $('.js-ajax-brand .js-ajax-brand-item');

	if (ajaxBrand.length > 0) {
		ajaxBrand.click(function (e) {
			e.preventDefault();

			var name = $(this).data('name');
			var id = $(this).data('id');

			var data = {
				'action': 'quote_ajax_brand_search_child',
				'search': '',
				'id': id,
				'name': name,
				'type': 'brand'
			};
			ajaxFunction(data);
		});
	}

	// click sub search product ajax
	$('body').on('click', '.js-ajax-sub-brand-item', function (e) {
		e.preventDefault();

		$('.js-search-ajax-content').height(400);

		var slug = $(this).data('slug');
		var name = $(this).data('name');
		var id = $(this).data('id');

		selectedList(id);

		var data = {
			'action': 'quote_ajax_products_search',
			'search': '',
			'slug': slug,
			'name': name,
			'type': 'brand'
		};
		ajaxFunction(data);
	});

	// click color item search ajax
	var ajaxColor = $('.js-ajax-color .js-ajax-color-item');

	if (ajaxColor.length > 0) {
		ajaxColor.click(function (e) {
			e.preventDefault();

			var slug = $(this).data('color');
			var name = $(this).data('name');
			var data = {
				'action': 'quote_ajax_products_search',
				'search': '',
				'slug': slug,
				'name': name,
				'type': 'range'
			};
			ajaxFunction(data);
		});
	}

	// click range item search ajax
	var ajaxRange = $('.js-ajax-range .js-ajax-range-item');

	if (ajaxRange.length > 0) {
		ajaxRange.click(function (e) {
			e.preventDefault();

			var slug = $(this).data('range');
			var name = $(this).data('name');
			var data = {
				'action': 'quote_ajax_products_search',
				'search': '',
				'slug': slug,
				'name': name,
				'type': 'range'
			};
			ajaxFunction(data);
		});
	}

	// Search name ajax
	$('.js-step2-search-button').click(function(e) {
		e.preventDefault();

		var name = $('.js-step2-search-input').val();
		var data = {
			'action': 'quote_ajax_products_search',
			'name': '',
			'search': name
		};
		ajaxFunction(data);
	});

	// search link
	$('.js-step2-search-link').click(function(e) {
		e.preventDefault();

		var name = $('.js-step2-search-input').val();
		var data = {
			'action': 'quote_ajax_products_search',
			'name': '',
			'search': name
		};
		ajaxFunction(data);
	});

	var showAjaxTitle = function(title) {
		var ajaxTitle = $('.js-search-ajax-title');
		ajaxTitle.parent().show();
		ajaxTitle.html(title);
	}

	var ajaxFunction = function(data) {
		var ajaxUrl = quote_vars.ajax_url;

		$('.js-spinner').fadeIn(300);
		$('.js-overlay').fadeIn(300);
		$('.js-search-ajax-content').html('');

		$('.js-step2-prev').addClass('ajax-active');

		$.ajax({
			type: 'POST',
			url: ajaxUrl,
			dataType: 'json',
			data: data,
			success: function (response) {
				if (response['success']) {
					var results = response['data'];
					if (data.name) {
						showAjaxTitle(data.name);
					}
					$('.js-step2-content-form').addClass('disabled');
					if (results) {
						$('.js-ajax-content').removeClass('disabled');
						$('.js-search-ajax-content').html(results);
					} else {
						// show not result products
						$('.js-search-ajax-no-results').show();
					}
				}
			}
		}).done(function() {
			setTimeout(function(){
				$('.js-spinner').fadeOut(300);
				$('.js-overlay').fadeOut(300);
			},500);
		});
	}

	var isEmail = function($email) {
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		return emailReg.test( $email );
	}

	$('.js-quote-submit-button').click(function() {

		// validate form before send ajax
		var contactName = $('.js-contact-name');
		if (contactName.val() === '') {
			contactName.addClass('error');
			return false;
		} else {
			contactName.removeClass('error');
		}

		var contactEmail = $('.js-contact-email');
		if (contactEmail.val() === '') {
			contactEmail.addClass('error');
			return false;
		} else {
			contactEmail.removeClass('error');
		}

		if (!isEmail(contactEmail.val())) {
			contactEmail.addClass('error');
			return false;
		} else {
			contactEmail.removeClass('error');
		}

		var contactPhone = $('.js-contact-phone');
		if(contactPhone.val() === '') {
			contactPhone.addClass('error');
			return false;
		} else {
			contactPhone.removeClass('error');
		}

		var contactAddress = $('.js-contact-address');
		if (contactAddress.val() === '') {
			contactAddress.addClass('error');
			return false;
		} else {
			contactAddress.removeClass('error');
		}

		$('.js-spinner-step7').fadeIn(300);
		$('.js-overlay-step7').fadeIn(300);

		var ajaxUrl = quote_vars.ajax_url;
		var json = JSON.stringify(push_data());
		var total_price = $('.js-quote-submit-button').data('total');
		// quote_contact_form
		var data = {
			'action': 'quote_contact_form',
			'json': json,
			'total_price': total_price,
			'contact_subject': $('.js-contact-subject').val(),
			'contact_body': $('.js-contact-body').val(),
			'contact_name': contactName.val(),
			'contact_email': contactEmail.val(),
			'contact_phone': contactPhone.val(),
			'contact_address': contactAddress.val(),
			'contact_project_begin': $('.js-contact-project-begin').find(":selected").val(),
			'contact_found_you_via': $('.js-contact-found-you-via').find(":selected").val(),
		};

		$.ajax({
			type: 'POST',
			url: ajaxUrl,
			dataType: 'json',
			data: data,
			success: function (response) {
				if (response['success']) {
					$('.step7-msg-success').removeClass('disabled');
					$('.js-quote-submit-button').addClass('disabled');
					$('.js-step7-next-button').removeClass('disabled');

					$('.js-step8-link-pdf').attr('href', response['pdf']);
				}
			}
		}).done(function() {
			setTimeout(function(){
				$('.js-spinner-step7').fadeOut(300);
				$('.js-overlay-step7').fadeOut(300);
			},500);
		});
	});

}); // jQuery
