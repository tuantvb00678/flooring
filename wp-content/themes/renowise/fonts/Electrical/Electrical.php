<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(

	'clamps (electrical set)' =>$set . '_f100',
	'electrical-panel-1 (electrical set)' =>$set . '_f101',
	'electrician-1 (electrical set)' =>$set . '_f102',
	'led-light (electrical set)' =>$set . '_f103',
	'fuse (electrical set)' =>$set . '_f104',
	'pliers (electrical set)' =>$set . '_f105',
	'socket (electrical set)' =>$set . '_f106',
	'plug (electrical set)' =>$set . '_f107',
	'battery-1 (electrical set)' =>$set . '_f108',
	'wrench (electrical set)' =>$set . '_f109',
	'ladder (electrical set)' =>$set . '_f10a',
	'drill (electrical set)' =>$set . '_f10b',
	'electric-meter (electrical set)' =>$set . '_f10c',
	'bulb-1 (electrical set)' =>$set . '_f10d',
	'hammer (electrical set)' =>$set . '_f10e',
	'cable (electrical set)' =>$set . '_f10f',
	'bulb (electrical set)' =>$set . '_f110',
	'ammeter (electrical set)' =>$set . '_f111',
	'toolbox (electrical set)' =>$set . '_f112',
	'electric-tower (electrical set)' =>$set . '_f113',
	'screwdriver (electrical set)' =>$set . '_f114',
	'multimeter (electrical set)' =>$set . '_f115',
	'cutter (electrical set)' =>$set . '_f116',
	'empty-battery (electrical set)' =>$set . '_f117',
	'copper-wire (electrical set)' =>$set . '_f118',
	'soldering-iron (electrical set)' =>$set . '_f119',
	'scotch (electrical set)' =>$set . '_f11a',
	'switch (electrical set)' =>$set . '_f11b',
	'flashlight (electrical set)' =>$set . '_f11c',
	'extension-cord (electrical set)' =>$set . '_f11d',
	'house (electrical set)' =>$set . '_f11e',
	'measuring-tape (electrical set)' =>$set . '_f11f',
	'electrician (electrical set)' =>$set . '_f120',
	'battery (electrical set)' =>$set . '_f121',
	'electrical-panel (electrical set)' =>$set . '_f122',
	'voltage-indicator (electrical set)' =>$set . '_f123',
	'electrician-service (electrical set)' =>$set . '_f124',
	'welding-machine (electrical set)' =>$set . '_f125',
	'gloves (electrical set)' =>$set . '_f126',
	'danger (electrical set)' =>$set . '_f127'
);