<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(

	'barrier (construction set)' =>$set . '_f100',
	'brick (construction set)' =>$set . '_f101',
	'brush (construction set)' =>$set . '_f102',
	'buildings (construction set)' =>$set . '_f103',
	'compass (construction set)' =>$set . '_f104',
	'cone (construction set)' =>$set . '_f105',
	'construction (construction set)' =>$set . '_f106',
	'crane (construction set)' =>$set . '_f107',
	'door (construction set)' =>$set . '_f108',
	'drill (construction set)' =>$set . '_f109',
	'electric-tower (construction set)' =>$set . '_f10a',
	'faucet (construction set)' =>$set . '_f10b',
	'fences (construction set)' =>$set . '_f10c',
	'gears (construction set)' =>$set . '_f10d',
	'hammer (construction set)' =>$set . '_f10e',
	'helmet (construction set)' =>$set . '_f10f',
	'ladder (construction set)' =>$set . '_f110',
	'loader (construction set)' =>$set . '_f111',
	'mask (construction set)' =>$set . '_f112',
	'measuring (construction set)' =>$set . '_f113',
	'parquet (construction set)' =>$set . '_f114',
	'pencil (construction set)' =>$set . '_f115',
	'pipe (construction set)' =>$set . '_f116',
	'plan (construction set)' =>$set . '_f117',
	'problem (construction set)' =>$set . '_f118',
	'roller (construction set)' =>$set . '_f119',
	'rulers (construction set)' =>$set . '_f11a',
	'screw (construction set)' =>$set . '_f11b',
	'screwdriver (construction set)' =>$set . '_f11c',
	'shovel (construction set)' =>$set . '_f11d',
	'tiles (construction set)' =>$set . '_f11e',
	'tool-box (construction set)' =>$set . '_f11f',
	'valve (construction set)' =>$set . '_f120',
	'wallpaper (construction set)' =>$set . '_f121',
	'wheelbarrow (construction set)' =>$set . '_f122',
	'36-window (construction set)' =>$set . '_f123'

);