<?php

// SECTION - OVERLAY, SPACINGS

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_section', 'background_overlay' );
}
if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_section', 'top_spacing' );
}
if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_section', 'bottom_spacing' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_section', array(
		array( 'param_name' => 'background_overlay', 'type' => 'dropdown', 'heading' => esc_html__( 'Background overlay', 'renowise' ), 'preview' => true, 'preview_strong' => true, 'group' => esc_html__( 'Design', 'renowise' ), 
			'value' => array(
				esc_html__( 'No overlay', 'renowise' )    	=> '',
				esc_html__( 'Light stripes', 'renowise' ) 	=> 'light_stripes',
				esc_html__( 'Dark stripes', 'renowise' )  	=> 'dark_stripes',
				esc_html__( 'Light solid', 'renowise' )	  	=> 'light_solid',
				esc_html__( 'Dark solid', 'renowise' )	  	=> 'dark_solid',
				esc_html__( 'Light gradient', 'renowise' )	=> 'light_gradient',
				esc_html__( 'Dark gradient', 'renowise' )	=> 'dark_gradient',
				esc_html__( 'Cross top', 'renowise' )	  	=> 'cross_top',
				esc_html__( 'Cross bottom', 'renowise' )	  	=> 'cross_bottom'
			)
		),
		array( 'param_name' => 'top_spacing', 'type' => 'dropdown', 'heading' => esc_html__( 'Top spacing', 'renowise' ), 'weight' => 1, 'preview' => true,
			'value' => array(
				esc_html__( 'No spacing', 'renowise' ) 	=> '',
				esc_html__( 'Extra small', 'renowise' ) 	=> 'extra_small',
				esc_html__( 'Small', 'renowise' ) 		=> 'small',		
				esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
				esc_html__( 'Medium', 'renowise' ) 		=> 'medium',
				esc_html__( 'Large', 'renowise' ) 		=> 'large',
				esc_html__( 'Extra large', 'renowise' ) 	=> 'extra_large',
				esc_html__( '5px', 'renowise' ) 			=> '5',
				esc_html__( '10px', 'renowise' ) 		=> '10',
				esc_html__( '15px', 'renowise' ) 		=> '15',
				esc_html__( '20px', 'renowise' ) 		=> '20',
				esc_html__( '25px', 'renowise' ) 		=> '25',
				esc_html__( '30px', 'renowise' ) 		=> '30',
				esc_html__( '35px', 'renowise' ) 		=> '35',
				esc_html__( '40px', 'renowise' ) 		=> '40',
				esc_html__( '45px', 'renowise' ) 		=> '45',
				esc_html__( '50px', 'renowise' ) 		=> '50',
				esc_html__( '60px', 'renowise' ) 		=> '60',
				esc_html__( '70px', 'renowise' ) 		=> '70',
				esc_html__( '80px', 'renowise' ) 		=> '80',
				esc_html__( '90px', 'renowise' ) 		=> '90',
				esc_html__( '100px', 'renowise' ) 		=> '100'
			)
		),
		array( 'param_name' => 'bottom_spacing', 'type' => 'dropdown', 'heading' => esc_html__( 'Bottom spacing', 'renowise' ), 'weight' => 2, 'preview' => true,
			'value' => array(
				esc_html__( 'No spacing', 'renowise' ) 	=> '',
				esc_html__( 'Extra small', 'renowise' ) 	=> 'extra_small',
				esc_html__( 'Small', 'renowise' ) 		=> 'small',		
				esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
				esc_html__( 'Medium', 'renowise' ) 		=> 'medium',
				esc_html__( 'Large', 'renowise' ) 		=> 'large',
				esc_html__( 'Extra large', 'renowise' ) 	=> 'extra_large',
				esc_html__( '5px', 'renowise' ) 			=> '5',
				esc_html__( '10px', 'renowise' ) 		=> '10',
				esc_html__( '15px', 'renowise' ) 		=> '15',
				esc_html__( '20px', 'renowise' ) 		=> '20',
				esc_html__( '25px', 'renowise' ) 		=> '25',
				esc_html__( '30px', 'renowise' ) 		=> '30',
				esc_html__( '35px', 'renowise' ) 		=> '35',
				esc_html__( '40px', 'renowise' ) 		=> '40',
				esc_html__( '45px', 'renowise' ) 		=> '45',
				esc_html__( '50px', 'renowise' ) 		=> '50',
				esc_html__( '60px', 'renowise' ) 		=> '60',
				esc_html__( '70px', 'renowise' ) 		=> '70',
				esc_html__( '80px', 'renowise' ) 		=> '80',
				esc_html__( '90px', 'renowise' ) 		=> '90',
				esc_html__( '100px', 'renowise' ) 		=> '100'
			)
		),
	));
}

function renowise_bt_bb_section_class( $class, $atts ) {
	if ( isset( $atts['background_overlay'] ) && $atts['background_overlay'] != '' ) {
		$class[] = 'bt_bb_background_overlay' . '_' . $atts['background_overlay'];
	}
	return $class;
}

add_filter( 'bt_bb_section_class', 'renowise_bt_bb_section_class', 10, 2 );


// ROW - NEGATIVE MARGIN & SHADOW

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_row', array(
		array( 'param_name' => 'negative_margin', 'type' => 'dropdown', 'heading' => esc_html__( 'Negative Margin', 'renowise' ), 'group' => esc_html__( 'General', 'renowise' ), 'preview' => true,
		'value' => array(
				esc_html__( 'No margin', 'renowise' ) 	=> '',
				esc_html__( 'Small', 'renowise' ) 		=> 'small',
				esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
				esc_html__( 'Medium', 'renowise' ) 		=> 'medium',
				esc_html__( 'Large', 'renowise' ) 		=> 'large',
				esc_html__( 'Extra Large', 'renowise' ) 	=> 'extralarge'
			)
		),
		array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Style', 'renowise' ), 
			'group' => esc_html__( 'General', 'renowise' ), 'preview' => true, 
			'value' => array(
				esc_html__( 'Default', 'renowise' ) 	=> '',
				esc_html__( 'Shadow', 'renowise' ) 	=> 'shadow',
				esc_html__( 'Border', 'renowise' ) 	=> 'border'
			)
		)
	));
}

function renowise_bt_bb_row_class( $class, $atts ) {
	if ( isset( $atts['negative_margin'] ) && $atts['negative_margin'] != '' ) {
		$class[] = 'bt_bb_negative_margin' . '_' . $atts['negative_margin'];
	}
	if ( isset( $atts['style'] ) && $atts['style'] != '' ) {
		$class[] = 'bt_bb_style' . '_' . $atts['style'];
	}
	return $class;
}

add_filter( 'bt_bb_row_class', 'renowise_bt_bb_row_class', 10, 2 );



// INNER ROW - NEGATIVE MARGIN, STYLE - SHADOW & BORDER

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_row_inner', array(
		array( 'param_name' => 'negative_margin', 'type' => 'dropdown', 'heading' => esc_html__( 'Negative Margin', 'renowise' ), 'group' => esc_html__( 'General', 'renowise' ), 'preview' => true,
		'value' => array(
				esc_html__( 'No margin', 'renowise' ) 	=> '',
				esc_html__( 'Small', 'renowise' ) 		=> 'small',
				esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
				esc_html__( 'Medium', 'renowise' ) 		=> 'medium',
				esc_html__( 'Large', 'renowise' ) 		=> 'large',
				esc_html__( 'Extra Large', 'renowise' ) 	=> 'extralarge'
			)
		),
		array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Style', 'renowise' ), 
			'group' => esc_html__( 'General', 'renowise' ), 'preview' => true, 
			'value' => array(
				esc_html__( 'Default', 'renowise' ) 	=> '',
				esc_html__( 'Shadow', 'renowise' ) 	=> 'shadow',
				esc_html__( 'Border', 'renowise' ) 	=> 'border'
			)
		)
	));
}

function renowise_bt_bb_row_inner_class( $class, $atts ) {
	if ( isset( $atts['negative_margin'] ) && $atts['negative_margin'] != '' ) {
		$class[] = 'bt_bb_negative_margin' . '_' . $atts['negative_margin'];
	}
	if ( isset( $atts['style'] ) && $atts['style'] != '' ) {
		$class[] = 'bt_bb_style' . '_' . $atts['style'];
	}
	return $class;
}

add_filter( 'bt_bb_row_inner_class', 'renowise_bt_bb_row_inner_class', 10, 2 );



// COLUMN - INNER PADDING, STYLE - SHADOW, BORDER

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_column', 'padding' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_column', array(
		array( 'param_name' => 'padding', 'type' => 'dropdown', 'heading' => esc_html__( 'Inner padding', 'renowise' ), 'preview' => true,
			'value' => array(
				esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
				esc_html__( 'Double', 'renowise' ) 		=> 'double',
				esc_html__( 'Text Indent', 'renowise' ) 	=> 'text_indent',
				esc_html__( '0px', 'renowise' ) 			=> '0',
				esc_html__( '5px', 'renowise' ) 			=> '5',
				esc_html__( '10px', 'renowise' ) 		=> '10',
				esc_html__( '15px', 'renowise' ) 		=> '15',
				esc_html__( '20px', 'renowise' ) 		=> '20',
				esc_html__( '25px', 'renowise' ) 		=> '25',
				esc_html__( '30px', 'renowise' ) 		=> '30',
				esc_html__( '35px', 'renowise' ) 		=> '35',
				esc_html__( '40px', 'renowise' ) 		=> '40',
				esc_html__( '45px', 'renowise' ) 		=> '45',
				esc_html__( '50px', 'renowise' ) 		=> '50',
				esc_html__( '60px', 'renowise' ) 		=> '60',
				esc_html__( '70px', 'renowise' ) 		=> '70',
				esc_html__( '80px', 'renowise' ) 		=> '80',
				esc_html__( '90px', 'renowise' ) 		=> '90',
				esc_html__( '100px', 'renowise' ) 		=> '100'
			)
		),
		array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Style', 'renowise' ), 
			'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true, 
			'value' => array(
				esc_html__( 'Default', 'renowise' ) 						=> '',
				esc_html__( 'Shadow', 'renowise' ) 						=> 'shadow',
				esc_html__( 'Strong shadow', 'renowise' ) 				=> 'strong_shadow',
				esc_html__( 'Inner shadow', 'renowise' ) 				=> 'inner_shadow',
				esc_html__( 'Strong inner shadow', 'renowise' ) 			=> 'strong_inner_shadow',
				esc_html__( 'Shadow on hover', 'renowise' ) 				=> 'hover_shadow',
				esc_html__( 'Border + Shadow on hover', 'renowise' ) 	=> 'border_shadow'
			)
		),
		array( 'param_name' => 'border', 'type' => 'dropdown', 'heading' => esc_html__( 'Border', 'renowise' ), 
			'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true, 
			'value' => array(
				esc_html__( 'None', 'renowise' ) 						=> '',
				esc_html__( 'Top Right + Bottom Left', 'renowise' ) 		=> 'top_right_bottom_left',
				esc_html__( 'Top Left + Bottom Right', 'renowise' ) 		=> 'top_left_bottom_right',
				esc_html__( 'Top Left', 'renowise' ) 					=> 'top_left',
				esc_html__( 'Top Right', 'renowise' ) 					=> 'top_right',
				esc_html__( 'Bottom Left', 'renowise' ) 					=> 'bottom_left',
				esc_html__( 'Bottom Right', 'renowise' ) 				=> 'bottom_right',
				esc_html__( 'Top Right + Bottom Right', 'renowise' ) 	=> 'top_right_bottom_right',
				esc_html__( 'Top Left + Bottom Left', 'renowise' ) 		=> 'top_left_bottom_left'
			)
		),
		array( 'param_name' => 'border_color', 'type' => 'dropdown', 'heading' => esc_html__( 'Border Color', 'renowise' ), 
			'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true, 
			'value' => array(
				esc_html__( 'Inherit', 'renowise' ) 		=> '',
				esc_html__( 'Accent', 'renowise' ) 		=> 'accent',
				esc_html__( 'Alternate', 'renowise' ) 	=> 'alternate',
				esc_html__( 'Light', 'renowise' ) 		=> 'light',
				esc_html__( 'Dark', 'renowise' ) 		=> 'dark',
				esc_html__( 'Gray', 'renowise' ) 		=> 'gray'
			)
		)
	));
}

function renowise_bt_bb_column_class( $class, $atts ) {
	if ( isset( $atts['style'] ) && $atts['style'] != '' ) {
		$class[] = 'bt_bb_style' . '_' . $atts['style'];
	}
	if ( isset( $atts['border'] ) && $atts['border'] != '' ) {
		$class[] = 'bt_bb_border' . '_' . $atts['border'];
	}
	if ( isset( $atts['border_color'] ) && $atts['border_color'] != '' ) {
		$class[] = 'bt_bb_border_color' . '_' . $atts['border_color'];
	}
	return $class;
}

add_filter( 'bt_bb_column_class', 'renowise_bt_bb_column_class', 10, 2 );



// INNER COLUMN - INNER PADDING, STYLE - SHADOW, BORDER

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_column_inner', 'padding' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_column_inner', array(
		array( 'param_name' => 'padding', 'type' => 'dropdown', 'heading' => esc_html__( 'Inner Padding', 'renowise' ), 'preview' => true,
			'value' => array(
				esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
				esc_html__( 'Double', 'renowise' ) 		=> 'double',
				esc_html__( 'Text Indent', 'renowise' ) 	=> 'text_indent',
				esc_html__( '0px', 'renowise' ) 			=> '0',
				esc_html__( '5px', 'renowise' ) 			=> '5',
				esc_html__( '10px', 'renowise' ) 		=> '10',
				esc_html__( '15px', 'renowise' ) 		=> '15',
				esc_html__( '20px', 'renowise' ) 		=> '20',
				esc_html__( '25px', 'renowise' ) 		=> '25',
				esc_html__( '30px', 'renowise' ) 		=> '30',
				esc_html__( '35px', 'renowise' ) 		=> '35',
				esc_html__( '40px', 'renowise' ) 		=> '40',
				esc_html__( '45px', 'renowise' ) 		=> '45',
				esc_html__( '50px', 'renowise' ) 		=> '50',
				esc_html__( '60px', 'renowise' ) 		=> '60',
				esc_html__( '70px', 'renowise' ) 		=> '70',
				esc_html__( '80px', 'renowise' ) 		=> '80',
				esc_html__( '90px', 'renowise' ) 		=> '90',
				esc_html__( '100px', 'renowise' ) 		=> '100'
			)
		),
		array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Style', 'renowise' ), 
			'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true, 
			'value' => array(
				esc_html__( 'Default', 'renowise' ) 						=> '',
				esc_html__( 'Shadow', 'renowise' ) 						=> 'shadow',
				esc_html__( 'Strong shadow', 'renowise' ) 				=> 'strong_shadow',
				esc_html__( 'Inner shadow', 'renowise' ) 				=> 'inner_shadow',
				esc_html__( 'Strong inner shadow', 'renowise' ) 			=> 'strong_inner_shadow',
				esc_html__( 'Shadow on hover', 'renowise' ) 				=> 'hover_shadow',
				esc_html__( 'Border + Shadow on hover', 'renowise' ) 	=> 'border_shadow'
			)
		),
		array( 'param_name' => 'border', 'type' => 'dropdown', 'heading' => esc_html__( 'Border', 'renowise' ), 
			'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true, 
			'value' => array(
				esc_html__( 'None', 'renowise' ) 						=> '',
				esc_html__( 'Top Right + Bottom Left', 'renowise' ) 		=> 'top_right_bottom_left',
				esc_html__( 'Top Left + Bottom Right', 'renowise' ) 		=> 'top_left_bottom_right',
				esc_html__( 'Top Left', 'renowise' ) 					=> 'top_left',
				esc_html__( 'Top Right', 'renowise' ) 					=> 'top_right',
				esc_html__( 'Bottom Left', 'renowise' ) 					=> 'bottom_left',
				esc_html__( 'Bottom Right', 'renowise' ) 				=> 'bottom_right',
				esc_html__( 'Top Right + Bottom Right', 'renowise' ) 	=> 'top_right_bottom_right',
				esc_html__( 'Top Left + Bottom Left', 'renowise' ) 		=> 'top_left_bottom_left'
			)
		),
		array( 'param_name' => 'border_color', 'type' => 'dropdown', 'heading' => esc_html__( 'Border Color', 'renowise' ), 
			'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true, 
			'value' => array(
				esc_html__( 'Inherit', 'renowise' ) 		=> '',
				esc_html__( 'Accent', 'renowise' ) 		=> 'accent',
				esc_html__( 'Alternate', 'renowise' ) 	=> 'alternate',
				esc_html__( 'Light', 'renowise' ) 		=> 'light',
				esc_html__( 'Dark', 'renowise' ) 		=> 'dark',
				esc_html__( 'Gray', 'renowise' ) 		=> 'gray'
			)
		)
	));
}

function renowise_bt_bb_column_inner_class( $class, $atts ) {
	if ( isset( $atts['style'] ) && $atts['style'] != '' ) {
		$class[] = 'bt_bb_style' . '_' . $atts['style'];
	}
	if ( isset( $atts['border'] ) && $atts['border'] != '' ) {
		$class[] = 'bt_bb_border' . '_' . $atts['border'];
	}
	if ( isset( $atts['border_color'] ) && $atts['border_color'] != '' ) {
		$class[] = 'bt_bb_border_color' . '_' . $atts['border_color'];
	}
	return $class;
}

add_filter( 'bt_bb_column_inner_class', 'renowise_bt_bb_column_inner_class', 10, 2 );



// ICON - SIZE, NEGATIVE MARGIN

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_icon', 'size' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_icon', array(
		array( 'param_name' => 'size', 'type' => 'dropdown', 'heading' => esc_html__( 'Size', 'renowise' ), 'preview' => true,
			'value' => array(
				esc_html__( 'Small', 'renowise' ) 		=> 'small',
				esc_html__( 'Extra small', 'renowise' ) 	=> 'xsmall',
				esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
				esc_html__( 'Medium', 'renowise' ) 		=> 'medium',
				esc_html__( 'Large', 'renowise' ) 		=> 'large',
				esc_html__( 'Extra large', 'renowise' ) 	=> 'xlarge'
			)
		)	
	) );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_icon', array(
		array( 'param_name' => 'negative_margin', 'type' => 'dropdown', 'heading' => esc_html__( 'Negative Margin', 'renowise' ), 'group' => esc_html__( 'Design', 'renowise' ), 'preview' => true,
			'value' => array(
				esc_html__( 'No', 'renowise' ) 		=> '',
				esc_html__( 'Small', 'renowise' ) 	=> 'small',
				esc_html__( 'Normal', 'renowise' ) 	=> 'normal',
				esc_html__( 'Medium', 'renowise' ) 	=> 'medium'
			)
		)
	));
}

function renowise_bt_bb_icon_class( $class, $atts ) {
	if ( isset( $atts['negative_margin'] ) && $atts['negative_margin'] != '' ) {
		$class[] = 'bt_bb_negative_margin' . '_' . $atts['negative_margin'];
	}
	return $class;
}

add_filter( 'bt_bb_icon_class', 'renowise_bt_bb_icon_class', 10, 2 );


// IMAGE - STYLE - BORDER, INNER PADDING

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_image', 'hover_style' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_image', array(
		array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Style', 'renowise' ), 'group' => esc_html__( 'General', 'renowise' ), 'preview' => true,
			'value' => array(
				esc_html__( 'Default', 'renowise' ) 	=> '',
				esc_html__( 'Border', 'renowise' ) 	=> 'border'
			)
		),
		array( 'param_name' => 'inner_padding', 'type' => 'dropdown', 'heading' => esc_html__( 'Inner Padding', 'renowise' ), 'default' => '15', 'group' => esc_html__( 'Content', 'renowise' ),
			'value' => array(
				esc_html__( 'Default', 'renowise' ) 	=> ' ',
				esc_html__( '0px', 'renowise' ) 		=> '0',
				esc_html__( '5px', 'renowise' ) 		=> '5',
				esc_html__( '10px', 'renowise' ) 	=> '10',
				esc_html__( '15px', 'renowise' ) 	=> '15',
				esc_html__( '20px', 'renowise' ) 	=> '20',
				esc_html__( '25px', 'renowise' ) 	=> '25',
				esc_html__( '30px', 'renowise' ) 	=> '30',
				esc_html__( '35px', 'renowise' ) 	=> '35',
				esc_html__( '40px', 'renowise' ) 	=> '40',
				esc_html__( '45px', 'renowise' ) 	=> '45',
				esc_html__( '50px', 'renowise' ) 	=> '50',
				esc_html__( '60px', 'renowise' ) 	=> '60',
				esc_html__( '70px', 'renowise' ) 	=> '70',
				esc_html__( '80px', 'renowise' ) 	=> '80',
				esc_html__( '90px', 'renowise' ) 	=> '90',
				esc_html__( '100px', 'renowise' ) 	=> '100'
			)
		),
		array( 'param_name' => 'hover_style', 'type' => 'dropdown', 'heading' => esc_html__( 'Mouse hover style', 'renowise' ), 'group' => esc_html__( 'URL', 'renowise' ),
			'value' => array(
				esc_html__( 'Simple', 'renowise' ) 					=> 'simple',
				esc_html__( 'Flip', 'renowise' ) 					=> 'flip',
				esc_html__( 'Zoom in', 'renowise' ) 					=> 'zoom-in',
				esc_html__( 'To grayscale', 'renowise' ) 			=> 'to-grayscale',
				esc_html__( 'From grayscale', 'renowise' ) 			=> 'from-grayscale',
				esc_html__( 'Zoom in to grayscale', 'renowise' ) 	=> 'zoom-in-to-grayscale',
				esc_html__( 'Zoom in from grayscale', 'renowise' ) 	=> 'zoom-in-from-grayscale',
				esc_html__( 'Scroll', 'renowise' ) 					=> 'scroll',
				esc_html__( 'Move up', 'renowise' ) 					=> 'move-up'
			)
		),
	));
}

function renowise_bt_bb_image_class( $class, $atts ) {
	if ( isset( $atts['style'] ) && $atts['style'] != '' ) {
		$class[] = 'bt_bb_style' . '_' . $atts['style'];
	}
	if ( isset( $atts['inner_padding'] ) && $atts['inner_padding'] != '' ) {
		$class[] = 'bt_bb_inner_padding' . '_' . $atts['inner_padding'];
	}
	return $class;
}

add_filter( 'bt_bb_image_class', 'renowise_bt_bb_image_class', 10, 2 );


// SEPARATOR - SPACINGS

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_separator', 'top_spacing' );
}
if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_separator', 'bottom_spacing' );
}
if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_separator', array(
		array( 'param_name' => 'top_spacing', 'type' => 'dropdown', 'heading' => esc_html__( 'Top spacing', 'renowise' ), 'weight' => 0, 'preview' => true,
			'value' => array(
				esc_html__( 'No spacing', 'renowise' ) 	=> '',
				esc_html__( 'Extra small', 'renowise' ) 	=> 'extra_small',
				esc_html__( 'Small', 'renowise' ) 		=> 'small',		
				esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
				esc_html__( 'Medium', 'renowise' )	 	=> 'medium',
				esc_html__( 'Large', 'renowise' ) 		=> 'large',
				esc_html__( 'Extra large', 'renowise' ) 	=> 'extra_large',
				esc_html__( '5px', 'renowise' ) 			=> '5',
				esc_html__( '10px', 'renowise' ) 		=> '10',
				esc_html__( '15px', 'renowise' ) 		=> '15',
				esc_html__( '20px', 'renowise' ) 		=> '20',
				esc_html__( '25px', 'renowise' ) 		=> '25',
				esc_html__( '30px', 'renowise' ) 		=> '30',
				esc_html__( '35px', 'renowise' ) 		=> '35',
				esc_html__( '40px', 'renowise' ) 		=> '40',
				esc_html__( '45px', 'renowise' ) 		=> '45',
				esc_html__( '50px', 'renowise' ) 		=> '50',
				esc_html__( '60px', 'renowise' )			=> '60',
				esc_html__( '70px', 'renowise' ) 		=> '70',
				esc_html__( '80px', 'renowise' ) 		=> '80',
				esc_html__( '90px', 'renowise' ) 		=> '90',
				esc_html__( '100px', 'renowise' ) 		=> '100'
			)
		),
		array( 'param_name' => 'bottom_spacing', 'type' => 'dropdown', 'heading' => esc_html__( 'Bottom spacing', 'renowise' ), 'weight' => 1, 'preview' => true,
			'value' => array(
				esc_html__( 'No spacing', 'renowise' ) 	=> '',
				esc_html__( 'Extra small', 'renowise' ) 	=> 'extra_small',
				esc_html__( 'Small', 'renowise' ) 		=> 'small',		
				esc_html__( 'Normal', 'renowise' ) 		=> 'normal',
				esc_html__( 'Medium', 'renowise' ) 		=> 'medium',
				esc_html__( 'Large', 'renowise' ) 		=> 'large',
				esc_html__( 'Extra large', 'renowise' ) 	=> 'extra_large',
				esc_html__( '5px', 'renowise' ) 			=> '5',
				esc_html__( '10px', 'renowise' ) 		=> '10',
				esc_html__( '15px', 'renowise' ) 		=> '15',
				esc_html__( '20px', 'renowise' ) 		=> '20',
				esc_html__( '25px', 'renowise' ) 		=> '25',
				esc_html__( '30px', 'renowise' ) 		=> '30',
				esc_html__( '35px', 'renowise' ) 		=> '35',
				esc_html__( '40px', 'renowise' ) 		=> '40',
				esc_html__( '45px', 'renowise' ) 		=> '45',
				esc_html__( '50px', 'renowise' ) 		=> '50',
				esc_html__( '60px', 'renowise' ) 		=> '60',
				esc_html__( '70px', 'renowise' ) 		=> '70',
				esc_html__( '80px', 'renowise' ) 		=> '80',
				esc_html__( '90px', 'renowise' ) 		=> '90',
				esc_html__( '100px', 'renowise' ) 		=> '100'
			)
		),
	) );
}


// CUSTOM MENU - ORIENTATION, CAPITALIZE

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_custom_menu', 'direction' );
}

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_custom_menu', array(
		array( 'param_name' => 'orientation', 'default' => 'vertical', 'type' => 'dropdown', 'heading' => esc_html__( 'Orientation', 'renowise' ), 'weight' => 1, 'preview' => true,
			'value' => array(
				esc_html__( 'Vertical', 'renowise' ) 	=> 'vertical',
				esc_html__( 'Horizontal', 'renowise' ) 	=> 'horizontal'
			)
		),
		array( 'param_name' => 'capitalize', 'default' => '', 'type' => 'dropdown', 'heading' => esc_html__( 'Capitalize Menu Items', 'renowise' ), 'weight' => 2, 'preview' => true,
			'value' => array(
				esc_html__( 'No', 'renowise' ) 			=> '',
				esc_html__( 'Yes', 'renowise' ) 			=> 'yes'
			)
		),
	));
}

function renowise_bt_bb_custom_menu_class( $class, $atts ) {
	if ( isset( $atts['orientation'] ) && $atts['orientation'] != '' ) {
		$class[] = 'bt_bb_orientation' . '_' . $atts['orientation'];
	}
	if ( isset( $atts['capitalize'] ) && $atts['capitalize'] != '' ) {
		$class[] = 'bt_bb_capitalize' . '_' . $atts['capitalize'];
	}
	return $class;
}

add_filter( 'bt_bb_custom_menu_class', 'renowise_bt_bb_custom_menu_class', 10, 2 );



// SLIDER - ARROWS POSITION, SIZE, DOTS, STYLE, COLOR SCHEME

if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_content_slider', 'arrows_size' );
}
if ( function_exists( 'bt_bb_remove_params' ) ) {
	bt_bb_remove_params( 'bt_bb_content_slider', 'show_dots' );
}

if ( is_file( dirname(__FILE__) . '/../../../../plugins/bold-page-builder/content_elements_misc/misc.php' ) ) {
	require_once( dirname(__FILE__) . '/../../../../plugins/bold-page-builder/content_elements_misc/misc.php' );	
}
if ( function_exists('bt_bb_get_color_scheme_param_array') ) {
	$color_scheme_arr = bt_bb_get_color_scheme_param_array();
} else {
	$color_scheme_arr = array();
}


if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_content_slider', array(
		array( 'param_name' => 'arrows_position', 'default' => '', 'weight' => 2, 'group' => esc_html__( 'Navigation', 'renowise' ), 'preview' => true, 'type' => 'dropdown', 'heading' => esc_html__( 'Navigation arrows position', 'renowise' ), 
			'value' => array(
				esc_html__( 'No arrows', 'renowise' ) 		=> 'no_arrows',
				esc_html__( 'Middle Inside', 'renowise' ) 	=> '',
				esc_html__( 'Middle Outside', 'renowise' ) 	=> 'middle-outside'
			)
		),
		array( 'param_name' => 'arrows_size', 'type' => 'dropdown', 'preview' => true, 'group' => esc_html__( 'Navigation', 'renowise' ), 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows size', 'renowise' ),
			'value' => array(
				esc_html__( 'No arrows', 'renowise' ) => 'no_arrows',
				esc_html__( 'Small', 'renowise' ) 	 => 'small',
				esc_html__( 'Normal', 'renowise' ) 	 => 'normal',
				esc_html__( 'Large', 'renowise' ) 	 => 'large'
			)
		),
		array( 'param_name' => 'show_dots', 'type' => 'dropdown', 'heading' => esc_html__( 'Dots navigation', 'renowise' ), 'group' => esc_html__( 'Navigation', 'renowise' ),
			'value' => array(
				esc_html__( 'Bottom', 'renowise' ) 	=> 'bottom',
				esc_html__( 'Below', 'renowise' ) 	=> 'below',
				esc_html__( 'Hide', 'renowise' ) 	=> 'hide'
			)
		),
		array( 'param_name' => 'arrows_style', 'default' => 'borderless', 'weight' => 3, 'preview' => true, 'type' => 'dropdown', 'heading' => esc_html__( 'Navigation arrows style', 'renowise' ), 'group' => esc_html__( 'Navigation', 'renowise' ),
			'value' => array(
				esc_html__( 'Filled', 'renowise' ) 		=> 'filled',
				esc_html__( 'Borderless', 'renowise' ) 	=> 'borderless'
			)
		),
		array( 'param_name' => 'color_scheme', 'preview' => true, 'type' => 'dropdown', 'weight' => 4, 'heading' => esc_html__( 'Navigation Color scheme', 'renowise' ), 'value' => $color_scheme_arr, 'group' => esc_html__( 'Navigation', 'renowise') 
		),
	));
}

function renowise_bt_bb_content_slider_class( $class, $atts ) {
	if ( isset( $atts['arrows_position'] ) && $atts['arrows_position'] != '' ) {
		$class[] = 'bt_bb_arrows_position' . '_' . $atts['arrows_position'];
	}
	if ( isset( $atts['arrows_style'] ) && $atts['arrows_style'] != '' ) {
		$class[] = 'bt_bb_arrows_style' . '_' . $atts['arrows_style'];
	}
	if ( isset( $atts['color_scheme'] ) && $atts['color_scheme'] != '' ) {
		$class[] = 'bt_bb_color_scheme' . '_' . bt_bb_get_color_scheme_id( $atts['color_scheme'] );
	}
	return $class;
}

add_filter( 'bt_bb_content_slider_class', 'renowise_bt_bb_content_slider_class', 10, 2 );


// SLIDER ITEM - STYLE

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_content_slider_item', array(
		array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Style', 'renowise' ), 'preview' => true, 
			'value' => array(
				esc_html__( 'Default', 'renowise' ) 						=> '',
				esc_html__( 'Gray background', 'renowise' ) 				=> 'gray',
				esc_html__( 'Gray background + border', 'renowise' ) 	=> 'gray_border',
				esc_html__( 'White background', 'renowise' ) 			=> 'white',
				esc_html__( 'White background + border', 'renowise' ) 	=> 'white_border'
			)
		),
	));
}

function renowise_bt_bb_content_slider_item_class( $class, $atts ) {
	if ( isset( $atts['style'] ) && $atts['style'] != '' ) {
		$class[] = 'bt_bb_style' . '_' . $atts['style'];
	}
	return $class;
}

add_filter( 'bt_bb_content_slider_item_class', 'renowise_bt_bb_content_slider_item_class', 10, 2 );


// IMAGE SLIDER

if ( function_exists( 'bt_bb_add_params' ) ) {
	bt_bb_add_params( 'bt_bb_slider', array(
		array( 'param_name' => 'gap', 'type' => 'dropdown', 'heading' => esc_html__( 'Gap', 'renowise' ),  
			'value' => array(
				esc_html__( 'No gap', 'renowise' ) 		=> '',
				esc_html__( 'Small gap', 'renowise' ) 	=> 'small'
			)
		),
		array( 'param_name' => 'arrow_style', 'type' => 'dropdown', 'heading' => esc_html__( 'Arrow style', 'renowise' ),  
			'value' => array(
				esc_html__( 'Borderless', 'renowise' ) 	=> '',
				esc_html__( 'Filled', 'renowise' ) 		=> 'filled'
			)
		)
	) );
}

function renowise_bt_bb_slider_class( $class, $atts ) {
	if ( isset( $atts['gap'] ) && $atts['gap'] != '' ) {
		$class[] = 'bt_bb_gap' . '_' . $atts['gap'];
	}
	if ( isset( $atts['arrow_style'] ) && $atts['arrow_style'] != '' ) {
		$class[] = 'bt_bb_arrow_style' . '_' . $atts['arrow_style'];
	}
	return $class;
}
add_filter( 'bt_bb_slider_class', 'renowise_bt_bb_slider_class', 10, 2 );