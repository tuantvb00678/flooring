<?php

/* Remove unused params */

remove_action( 'customize_register', 'boldthemes_customize_hide_headline' );
remove_action( 'boldthemes_customize_register', 'boldthemes_customize_hide_headline' );



// DEFAULT HEADLINE

BoldThemes_Customize_Default::$data['default_headline'] = 'show';

if ( ! function_exists( 'boldthemes_customize_default_headline' ) ) {
	function boldthemes_customize_default_headline( $wp_customize ) {
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[default_headline]', array(
			'default'           => BoldThemes_Customize_Default::$data['default_headline'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'boldthemes_sanitize_select'
		));

		$wp_customize->add_control( 'default_headline', array(
			'label'     => esc_html__( 'Default Headline', 'renowise' ),
			'section'   => BoldThemesFramework::$pfx . '_header_footer_section',
			'settings'  => BoldThemesFramework::$pfx . '_theme_options[default_headline]',
			'priority'  => 95,
			'type'      => 'select',
			'choices'   => array(
				'hide'			=> esc_html__( 'Hide', 'renowise' ),
				'show' 			=> esc_html__( 'Show', 'renowise' ),
				'show-square' 	=> esc_html__( 'Show with square', 'renowise' )
			)
		));
	}
}
add_action( 'customize_register', 'boldthemes_customize_default_headline' );
add_action( 'boldthemes_customize_register', 'boldthemes_customize_default_headline' );


// HEADING WEIGHT

BoldThemes_Customize_Default::$data['default_heading_weight'] = 'default';

if ( ! function_exists( 'boldthemes_customize_default_heading_weight' ) ) {
	function boldthemes_customize_default_heading_weight( $wp_customize ) {

		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[default_heading_weight]', array(
			'default'           => BoldThemes_Customize_Default::$data['default_heading_weight'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'boldthemes_sanitize_select'
		));
		$wp_customize->add_control( 'default_heading_weight', array(
			'label'     => esc_html__( 'Heading Weight', 'renowise' ),
			'section'   => BoldThemesFramework::$pfx . '_typo_section',
			'settings'  => BoldThemesFramework::$pfx . '_theme_options[default_heading_weight]',
			'priority'  => 100,
			'type'      => 'select',
			'choices'   => array(
				'default'	=> esc_html__( 'Default', 'renowise' ),
				'thin' 		=> esc_html__( 'Thin', 'renowise' ),
				'lighter' 	=> esc_html__( 'Lighter', 'renowise' ),
				'light' 	=> esc_html__( 'Light', 'renowise' ),
				'normal' 	=> esc_html__( 'Normal', 'renowise' ),
				'semi-bold' => esc_html__( 'Semi bold', 'renowise' ),
				'bold' 		=> esc_html__( 'Bold', 'renowise' ),
				'bolder' 	=> esc_html__( 'Bolder', 'renowise' )
			)
		));
	}
}
add_action( 'customize_register', 'boldthemes_customize_default_heading_weight' );
add_action( 'boldthemes_customize_register', 'boldthemes_customize_default_heading_weight' );

// CAPITALIZE MAIN MENU

BoldThemes_Customize_Default::$data['capitalize_main_menu'] = true;
if ( ! function_exists( 'boldthemes_customize_capitalize_main_menu' ) ) {
	function boldthemes_customize_capitalize_main_menu( $wp_customize ) {
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[capitalize_main_menu]', array(
			'default'           => BoldThemes_Customize_Default::$data['capitalize_main_menu'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'boldthemes_sanitize_checkbox'
		));
		$wp_customize->add_control( 'capitalize_main_menu', array(
			'label'     => esc_html__( 'Capitalize Main Menu Items', 'renowise' ),
			'section'   => BoldThemesFramework::$pfx . '_typo_section',
			'settings'  => BoldThemesFramework::$pfx . '_theme_options[capitalize_main_menu]',
			'priority'  => 150,
			'type'      => 'checkbox'
		));
	}
}

add_action( 'customize_register', 'boldthemes_customize_capitalize_main_menu' );
add_action( 'boldthemes_customize_register', 'boldthemes_customize_capitalize_main_menu' );

/* Helper function */

if ( ! function_exists( 'renowise_body_class' ) ) {
	function renowise_body_class( $extra_class ) {
		if ( boldthemes_get_option( 'default_heading_weight' ) ) {
			$extra_class[] =  'btHeadingWeight' . boldthemes_convert_param_to_camel_case ( boldthemes_get_option( 'default_heading_weight' ) );
		}
		return $extra_class;
	}
}


/* GENERAL */

// PAGE BACKGROUND COLOR

BoldThemes_Customize_Default::$data['page_background_color'] = '';

if ( ! function_exists( 'boldthemes_customize_page_background_color' ) ) {
	function boldthemes_customize_page_background_color( $wp_customize ) {
		
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[page_background_color]', array(
			'default'           => BoldThemes_Customize_Default::$data['page_background_color'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_hex_color'
		));
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_background_color', array(
			'label'    => esc_html__( 'Page Background Color', 'renowise' ),
			'section'  => BoldThemesFramework::$pfx . '_general_section',
			'settings' => BoldThemesFramework::$pfx . '_theme_options[page_background_color]',
			'priority' => 27,
			'context'  => BoldThemesFramework::$pfx . '_page_background_color'
		)));
	}
}
add_action( 'customize_register', 'boldthemes_customize_page_background_color' );
add_action( 'boldthemes_customize_register', 'boldthemes_customize_page_background_color' );

/* Helper function */

if ( ! function_exists( 'renowise_body_class' ) ) {
	function renowise_body_class( $extra_class ) {
		if ( boldthemes_get_option( 'page_background_color' ) ) {
			$extra_class[] =  'btPageBackground';
		}
		return $extra_class;
	}
}