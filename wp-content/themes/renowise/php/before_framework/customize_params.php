<?php

// HEADER STYLE

if ( ! function_exists( 'boldthemes_customize_header_style' ) ) {
	function boldthemes_customize_header_style( $wp_customize ) {
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[header_style]', array(
			'default'           => BoldThemes_Customize_Default::$data['header_style'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'boldthemes_sanitize_select'
		));
		$wp_customize->add_control( 'header_style', array(
			'label'     => esc_html__( 'Header Style', 'renowise' ),
			'description'    => esc_html__( 'Select header style for all the pages on the site.', 'renowise' ),
			'section'   => BoldThemesFramework::$pfx . '_header_footer_section',
			'settings'  => BoldThemesFramework::$pfx . '_theme_options[header_style]',
			'priority'  => 62,
			'type'      => 'select',
			'choices'   => array(
				'transparent-light'  	=> esc_html__( 'Transparent Light', 'renowise' ),
				'transparent-dark'   	=> esc_html__( 'Transparent Dark', 'renowise' ),
				'opacity-light'   		=> esc_html__( 'Opacity Light', 'renowise' ),
				'light-accent' 			=> esc_html__( 'Light + Accent', 'renowise' ),
				'dark-accent' 			=> esc_html__( 'Dark + Accent', 'renowise' ),
				'light-dark' 			=> esc_html__( 'Light + Dark', 'renowise' ),
				'accent-dark' 			=> esc_html__( 'Accent + Dark', 'renowise' ),
				'accent-light' 			=> esc_html__( 'Accent + Light', 'renowise' ),
				'grey-light' 			=> esc_html__( 'Grey + Light', 'renowise' ),
				'light-alternate' 		=> esc_html__( 'Light + Alternate', 'renowise' )
			)
		));
	}
}


