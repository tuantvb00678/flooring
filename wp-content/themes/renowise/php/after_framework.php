<?php

BoldThemes_Customize_Default::$data['body_font'] = 'Roboto';
BoldThemes_Customize_Default::$data['heading_supertitle_font'] = 'Montserrat';
BoldThemes_Customize_Default::$data['heading_font'] = 'Montserrat';
BoldThemes_Customize_Default::$data['heading_subtitle_font'] = 'Montserrat';
BoldThemes_Customize_Default::$data['menu_font'] = 'Montserrat';
BoldThemes_Customize_Default::$data['buttons_shape'] = 'hard-rounded';


BoldThemes_Customize_Default::$data['accent_color'] = '#f06731';
BoldThemes_Customize_Default::$data['alternate_color'] = '#4b4b4b';
BoldThemes_Customize_Default::$data['logo_height'] = '90';

BoldThemes_Customize_Default::$data['template_skin'] = false;


require_once( get_template_directory() . '/php/after_framework/functions.php' );
require_once( get_template_directory() . '/php/after_framework/customize_params.php' );


