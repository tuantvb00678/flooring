<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

define( 'WP_MEMORY_LIMIT', '256M' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'anews' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Yc!m}_IzYY|7(i@-1MXO@t4JdpX9zwJvD#3kRn2v?If^[<G)0IS{~r)BC{]zi}Z4' );
define( 'SECURE_AUTH_KEY',  'mMotgptZ==-(Ka*?#?YiTKZkTV!e^7yeEH3FOHX/H*WEiudn9Io%0>aF6o0!|?i^' );
define( 'LOGGED_IN_KEY',    'Rcf{~~b_*q!8g,*?u64m^~d9|ai:;91T^bBLwCY|#=b-afBv%O0(0 /d*k&,<=2-' );
define( 'NONCE_KEY',        '$X@a>1Lx2=y04=[OSh /=7]K2u]rJM!TRgJpf=E$:l:/nQ`[P>}VQvew4E@Vo4[e' );
define( 'AUTH_SALT',        '<&n1ZFr3B;Q#P0UV1@ZO5Tdsv(76#`BO;hS[_]e7x-/hC+r(x44Ae`HIu%d0@rm3' );
define( 'SECURE_AUTH_SALT', '%RpwrFv7p@iBvThbHq2#sO)8t584ZlD~,@;L7]yh4--;%[syW!I7~>(h:]C!>ej:' );
define( 'LOGGED_IN_SALT',   'k8?NZCNaw#fAj/(*VRp42qLBmI&4WX(E)c2zA#MI2/`krDo5mXtj8XJC61uJ#@@g' );
define( 'NONCE_SALT',       ',-n=-~df)X}Ck#Q)EJ8mTefZZAWIh@N1y6@w?;z9Qk<%e^QOl2Qu]o]O_9826_+G' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
